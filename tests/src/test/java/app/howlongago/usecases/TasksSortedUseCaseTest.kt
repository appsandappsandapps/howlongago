package app.howlongago.usecases

import app.howlongago.db.TaskWithInstance
import app.howlongago.usecases.TasksSortedUseCase
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.*

import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.usecases.repositories.TasksRepository
import app.howlongago.usecases.repositories.TasksSortingRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

class TasksSortedUseCaseTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var tasksRepo: TasksRepository

  @MockK(relaxed = true)
  lateinit var sortingRepo: TasksSortingRepository

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test insert()`() = runTest {
    coEvery { tasksRepo.insert("taskName") } returns 100
    coEvery { sortingRepo.selectedSort.value } returns SortEnum.DATE_DESC

    val returnedTaskId = TasksSortedUseCase(tasksRepo, sortingRepo)
      .taskInsert("taskName")

    coVerify(exactly = 1) { tasksRepo.insert("taskName") }
    assertEquals(100, returnedTaskId)
  }

  @Test
  fun `test setTasksSort()`() = runTest {
    coEvery { sortingRepo.selectedSort.value } returns SortEnum.DATE_ASC

    val tasksUseCase = TasksSortedUseCase(tasksRepo, sortingRepo)
    tasksUseCase.setTaskSort(SortEnum.DATE_ASC)

    coVerify(exactly = 1) { sortingRepo.setTaskSort(SortEnum.DATE_ASC) }
    coVerify(exactly = 1) { tasksRepo.getAllDateAsc() }
  }

  @Test
  fun `test getAllSortedIntoMutableState() date desc null-date sorted`() = runTest {
    val taskWithDate = TaskWithInstance(1, "a task name", 1, null)
    val taskWithoutDate = TaskWithInstance(1, "a task name", null, null)
    val tasks: List<TaskWithInstance> = listOf(taskWithDate, taskWithoutDate)
    coEvery { sortingRepo.selectedSort.value } returns SortEnum.DATE_DESC
    coEvery { tasksRepo.getAllDateDesc() } returns tasks

    val tasksUseCase = TasksSortedUseCase(tasksRepo, sortingRepo)
    tasksUseCase.getAllSortedIntoMutableState()
    val returnedTasks = tasksUseCase.tasks.first()

    assertEquals(taskWithoutDate, returnedTasks[0])
    assertEquals(taskWithDate, returnedTasks[1])
  }

  @Test
  fun `test getAllSortedIntoMutableState() date asc null-date sorted`() = runTest {
    val taskWithDate = TaskWithInstance(1, "a task name", 1, null)
    val taskWithoutDate = TaskWithInstance(1, "a task name", null, null)
    val tasks: List<TaskWithInstance> = listOf(taskWithDate, taskWithoutDate)
    coEvery { sortingRepo.selectedSort.value } returns SortEnum.DATE_ASC
    coEvery { tasksRepo.getAllDateAsc() } returns tasks

    val tasksUseCase = TasksSortedUseCase(tasksRepo, sortingRepo)
    tasksUseCase.getAllSortedIntoMutableState()
    val returnedTasks = tasksUseCase.tasks.first()

    assertEquals(taskWithoutDate, returnedTasks[0])
    assertEquals(taskWithDate, returnedTasks[1])
  }

  @Test
  fun `test getAllSortedIntoMutableState() name asc null-date not sorted`() = runTest {
    val taskWithDate = TaskWithInstance(1, "a task name", 1, null)
    val taskWithoutDate = TaskWithInstance(1, "a task name", null, null)
    val tasks: List<TaskWithInstance> = listOf(taskWithDate, taskWithoutDate)
    coEvery { sortingRepo.selectedSort.value } returns SortEnum.NAME_ASC
    coEvery { tasksRepo.getAllNameAsc() } returns tasks

    val tasksUseCase = TasksSortedUseCase(tasksRepo, sortingRepo)
    tasksUseCase.getAllSortedIntoMutableState()
    val returnedTasks = tasksUseCase.tasks.first()

    assertEquals(taskWithDate, returnedTasks[0])
    assertEquals(taskWithoutDate, returnedTasks[1])
  }

  @Test
  fun `test getAllSortedIntoMutableState() name desc null-date not sorted`() = runTest {
    val taskWithDate = TaskWithInstance(1, "a task name", 1, null)
    val taskWithoutDate = TaskWithInstance(1, "a task name", null, null)
    val tasks: List<TaskWithInstance> = listOf(taskWithDate, taskWithoutDate)
    coEvery { sortingRepo.selectedSort.value } returns SortEnum.NAME_DESC
    coEvery { tasksRepo.getAllNameDesc() } returns tasks

    val tasksUseCase = TasksSortedUseCase(tasksRepo, sortingRepo)
    tasksUseCase.getAllSortedIntoMutableState()
    val returnedTasks = tasksUseCase.tasks.first()

    assertEquals(taskWithDate, returnedTasks[0])
    assertEquals(taskWithoutDate, returnedTasks[1])
  }

}
