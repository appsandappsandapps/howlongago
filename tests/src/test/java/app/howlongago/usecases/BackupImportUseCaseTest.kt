package app.howlongago.usecases

import app.howlongago.TaskWithInstancesBackup
import app.howlongago.db.Instance
import app.howlongago.db.Task
import app.howlongago.db.TaskWithInstance
import app.howlongago.db.TasksBackup
import app.howlongago.usecases.TaskDetailsUseCase
import app.howlongago.usecases.TasksSortedUseCase
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.*

import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.usecases.repositories.InstancesRepository
import app.howlongago.usecases.repositories.TasksRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

class BackupImportUseCaseTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var tasksRepo: TasksRepository

  @MockK(relaxed = true)
  lateinit var instancesRepo: InstancesRepository

  @MockK(relaxed = true)
  lateinit var tasksSortedUseCase: TasksSortedUseCase

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test export()`() = runTest {
    val tasksBackup = listOf(
      TasksBackup(0, "a task", null, null, null, null, null),
      TasksBackup(1, "task2", 1, 1, null, 100, "note"),
      TasksBackup(1, "task2", 2, 1, null, 200, "another")
    )
    coEvery { tasksRepo.tasksBackup() } returns tasksBackup

    val db = BackupImportUseCase(tasksRepo, instancesRepo, tasksSortedUseCase)
      .exportDatabase()

    assertEquals("""[{"task":{"uid":0,"taskName":"a task"},"instances":[]},{"task":{"uid":1,"taskName":"task2"},"instances":[{"uid":1,"taskId":1,"date":100,"notes":"note"},{"uid":2,"taskId":1,"date":200,"notes":"another"}]}]""", db)
  }

  @Test
  fun `test import()`() = runTest {
    val db = """[{"task":{"uid":0,"taskName":"a task"},"instances":[]},{"task":{"uid":1,"taskName":"task2"},"instances":[{"uid":1,"taskId":1,"date":100,"notes":"note"},{"uid":2,"taskId":1,"date":200,"notes":"another"}]}]"""

    BackupImportUseCase(tasksRepo, instancesRepo, tasksSortedUseCase)
      .importDatabase(db)
    advanceUntilIdle()

    coVerify { tasksRepo.insert("a task") }
    coVerify { tasksRepo.insert("task2") }
    coVerify { instancesRepo.insert(0, 100, "note") }
    coVerify { instancesRepo.insert(0, 200, "another") }
    coVerify { tasksSortedUseCase.getAllSortedIntoMutableState() }
  }

  @Test
  fun `test import() with runtime exception`() = runTest {
    val db = """hmm"""

    var exceptionThrown = false
    try {
      BackupImportUseCase(tasksRepo, instancesRepo, tasksSortedUseCase)
        .importDatabase(db)
    } catch(e: Exception ) {
      exceptionThrown = true
    }

    assertEquals(true, exceptionThrown)
  }

}
