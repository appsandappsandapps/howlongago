package app.howlongago.usecases.repositories

import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.*

import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.usecases.datasources.TaskSortingDatasource
import app.howlongago.usecases.repositories.TasksSortingRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

class TasksSortingRepositoryTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var tasksSortingDatasource: TaskSortingDatasource

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test initial taskSort()`() = runTest {
    every { tasksSortingDatasource.getSort() } returns SortEnum.DATE_DESC

    val returnedDefaultSort = TasksSortingRepository(tasksSortingDatasource)
      .selectedSort.first()

    verify(exactly = 1) { tasksSortingDatasource.getSort() }
    assertEquals(SortEnum.DATE_DESC, returnedDefaultSort)
  }

  @Test
  fun `test setTaskSort()`() = runTest {
    val sortWeAreSetting = SortEnum.NAME_ASC
    every { tasksSortingDatasource.getSort() } returns SortEnum.DATE_DESC
    val sortingRepo = TasksSortingRepository(tasksSortingDatasource)

    sortingRepo.setTaskSort(sortWeAreSetting)
    val returnedSort = sortingRepo.selectedSort.first()

    verify(exactly = 1) { tasksSortingDatasource.setSort(sortWeAreSetting) }
    assertEquals(sortWeAreSetting, returnedSort)
  }


}
