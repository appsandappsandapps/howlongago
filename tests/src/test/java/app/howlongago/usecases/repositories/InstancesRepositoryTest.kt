package app.howlongago.usecases.repositories

import app.howlongago.db.TasksAndInstancesQueries
import com.squareup.sqldelight.Query
import com.squareup.sqldelight.TransactionWithReturn
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith

import app.howlongago.db.*
import app.howlongago.delayLong
import app.howlongago.usecases.repositories.InstancesRepository
import app.howlongago.usecases.repositories.TasksRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkObject
import io.mockk.verify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

class InstancesRepositoryTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK
  lateinit var dbInstanceQuery: Query<Instance>

  @MockK(relaxed = true)
  lateinit var db: TasksAndInstancesQueries

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test getAll()`() = runTest {
    val taskId = 1L
    val instances:List<Instance> = listOf()
    every { db.getInstances(1) } returns dbInstanceQuery
    every { dbInstanceQuery.executeAsList() } returns instances

    val returnedInstances = InstancesRepository(db).getAll(taskId)

    verify(exactly = 1) { db.getInstances(1L) }
    verify(exactly = 1) { dbInstanceQuery.executeAsList() }
    assertEquals(instances, returnedInstances)
  }

  @Test
  fun `test delete()`() = runTest {
    InstancesRepository(db).delete(1)

    verify(exactly = 1) { db.deleteInstance(1) }
  }

  @Test
  fun `test insert() with taskId only`() = runTest {
    val latestInsertQueryLong: Query<Long> = mockk()
    every { db.latestInstance() } returns latestInsertQueryLong
    every { latestInsertQueryLong.executeAsOne() } returns 200
    every { db.transactionWithResult<Long>(any(), any()) } answers { inv ->
      (inv.invocation.args[1] as (TransactionWithReturn<Long>.() -> Long))(mockk())
    }

    val returnedInstanceId = InstancesRepository(db).insert(1L, 100L)

    verify(exactly = 1) { db.insertInstance(uid = null, task_id = 1, date = 100, notes = "", nextDate = null) }
    assertEquals(200, returnedInstanceId)
  }

  @Test
  fun `test insert() with all data`() = runTest {
    val latestInsertQueryLong: Query<Long> = mockk()
    every { db.latestInstance() } returns latestInsertQueryLong
    every { latestInsertQueryLong.executeAsOne() } returns 200
    every { db.transactionWithResult<Long>(any(), any()) } answers { inv ->
      (inv.invocation.args[1] as (TransactionWithReturn<Long>.() -> Long))(mockk())
    }

    val returnedInstanceId = InstancesRepository(db).insert(1L, 100L, "a note")

    verify(exactly = 1) { db.insertInstance(uid = null, task_id = 1, date = 100, notes = "a note", nextDate = null) }
    assertEquals(200, returnedInstanceId)
  }

  @Test
  fun `test updateDate()`() = runTest {
    val instanceId = 100L
    val instance = Instance(instanceId, 0, 0, "a note", null)
    every { db.getInstance(instanceId) } returns dbInstanceQuery
    every { dbInstanceQuery.executeAsOne() } returns instance

    InstancesRepository(db).updateDate(instanceId, 1000)

    verify(exactly = 1) { db.getInstance(instanceId) }
    verify(exactly = 1) { dbInstanceQuery.executeAsOne() }
    verify(exactly = 1) { db.updateInstance(uid = instanceId, nextDate = null, notes = "a note", date = 1000,  ) }
  }

  @Test
  fun `test updateNotesDelayed()`() = runTest {
    val instanceId = 100L
    val instance = Instance(instanceId, 0, 0, "a note", null)
    val job: Job = mockk(relaxed = true)
    val onComplete: suspend () -> Unit = mockk(relaxed = true)
    every { db.getInstance(instanceId) } returns dbInstanceQuery
    every { dbInstanceQuery.executeAsOne() } returns instance
    mockkStatic(::delayLong)

    InstancesRepository(db, CoroutineScope(testDispatcher), job)
      .updateNoteDelayed(instanceId, "new note", onComplete)

    advanceUntilIdle()
    verify(exactly = 1) { job.cancel() }
    coVerify(exactly = 1) { delayLong(400) }
    verify(exactly = 1) { db.updateInstance(uid = instanceId, nextDate = null, notes = "new note", date = 0 ) }
    coVerify(exactly = 1) { onComplete.invoke() }
  }

}
