package app.howlongago.usecases.repositories

import app.howlongago.TaskBackup
import app.howlongago.db.TasksAndInstancesQueries
import com.squareup.sqldelight.Query
import com.squareup.sqldelight.TransactionWithReturn
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith

import app.howlongago.db.*
import app.howlongago.delayLong
import app.howlongago.usecases.repositories.TasksRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkObject
import io.mockk.verify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

class TasksRepositoryTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK
  lateinit var dbTaskWithInstanceQuery: Query<TaskWithInstance>

  @MockK
  lateinit var dbTaskQuery: Query<Task>

  @MockK(relaxed = true)
  lateinit var db: TasksAndInstancesQueries

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test get()`() = runTest {
    val task = Task(0, "")
    every { db.getTask(1) } returns dbTaskQuery
    every { dbTaskQuery.executeAsOne() } returns task

    val returnedTask = TasksRepository(db).get(1)

    assertEquals(task, returnedTask)
  }

  @Test
  fun `test getAllDateDesc()`() = runTest {
    val tasks: List<TaskWithInstance> = listOf()
    every { db.getTasksByInstanceDateDesc() } returns dbTaskWithInstanceQuery
    every { dbTaskWithInstanceQuery.executeAsList() } returns tasks

    val returnedTasks = TasksRepository(db).getAllDateDesc()

    assertEquals(tasks, returnedTasks)
  }

  @Test
  fun `test getAllDateAsc()`() = runTest {
    val tasks: List<TaskWithInstance> = listOf()
    every { db.getTasksByInstanceDateAsc() } returns dbTaskWithInstanceQuery
    every { dbTaskWithInstanceQuery.executeAsList() } returns tasks

    val returnedTasks = TasksRepository(db).getAllDateAsc()

    assertEquals(tasks, returnedTasks)
  }

  @Test
  fun `test getAllNameAsc()`() = runTest {
    val tasks: List<TaskWithInstance> = listOf()
    every { db.getTasksByNameAsc() } returns dbTaskWithInstanceQuery
    every { dbTaskWithInstanceQuery.executeAsList() } returns tasks

    val returnedTasks = TasksRepository(db).getAllNameAsc()

    assertEquals(tasks, returnedTasks)
  }

  @Test
  fun `test getAllNameDesc()`() = runTest {
    val tasks: List<TaskWithInstance> = listOf()
    every { db.getTasksByNameDesc() } returns dbTaskWithInstanceQuery
    every { dbTaskWithInstanceQuery.executeAsList() } returns tasks

    val returnedTasks = TasksRepository(db).getAllNameDesc()

    assertEquals(tasks, returnedTasks)
  }

  @Test
  fun `test delete()`() = runTest {
    TasksRepository(db).deleteWithInstances(1)

    verify(exactly = 1) { db.deleteTask(1) }
    verify(exactly = 1) { db.deleteInstanceByTask(1) }
  }

  @Test
  fun `test insert()`() = runTest {
    val taskId = 100L
    val latestInsertQueryLong: Query<Long> = mockk()
    every { db.transactionWithResult<Long>(any(), any()) } answers {
      (it.invocation.args[1] as (TransactionWithReturn<Long>.() -> Long))(mockk())
    }
    every { db.latestInstance() } returns latestInsertQueryLong
    every { latestInsertQueryLong.executeAsOne() } returns taskId

    val returnedTaskId = TasksRepository(db).insert(" taskName ")

    verify(exactly = 1) { db.insertTask(null, "taskName") }
    assertEquals(taskId, returnedTaskId)
  }

  @Test
  fun `test updateNameDelayed()`() = runTest {
    val job: Job = mockk(relaxed = true)
    val onComplete: suspend () -> Unit = mockk(relaxed = true)
    mockkStatic(::delayLong)

    TasksRepository(db, CoroutineScope(testDispatcher), job)
      .updateNameDelayed(1, " taskName ", onComplete)

    advanceUntilIdle()
    verify(exactly = 1) { job.cancel() }
    coVerify(exactly = 1) { delayLong(250) }
    verify(exactly = 1) { db.updateTask("taskName", 1) }
    coVerify(exactly = 1) { onComplete.invoke() }
  }

  @Test
  fun `test tasksBackup()`() = runTest {
    val backupTasks: List<TasksBackup> = listOf(TasksBackup(0, "", 0, 0, null, null, null))
    val queryTasksBackup: Query<TasksBackup> = mockk()
    every { db.tasksBackup() } returns queryTasksBackup
    every { queryTasksBackup.executeAsList() } returns backupTasks

    val returnedBackup = TasksRepository(db).tasksBackup()

    assertEquals(backupTasks, returnedBackup)
  }

}
