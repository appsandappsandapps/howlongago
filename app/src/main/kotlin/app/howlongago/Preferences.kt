package app.howlongago

import android.content.SharedPreferences
import kotlinx.coroutines.flow.MutableStateFlow

object Preferences {

  lateinit var darkTheme: MutableStateFlow<Boolean>

  fun setDarkTheme(themeDark: Boolean) {
    with(Application.instance!!.editPrefs) {
      putBoolean("darkMode", themeDark)
      apply()
      darkTheme.value = themeDark
    }
  }

  fun init() {
    val app = Application.instance!!
    val darkThemeDefault: Boolean = app.prefs.getBoolean("darkMode", true)
    darkTheme = MutableStateFlow(darkThemeDefault)
  }

}
