package app.howlongago

import android.app.Application

class Application : Application() {

  companion object {
    var instance: app.howlongago.Application? = null
  }

  override fun onCreate() {
    super.onCreate()
    instance = this
    ServiceLocator.init(applicationContext)
    Preferences.init()
  }

}
