package app.howlongago.ui.theme

import android.util.DisplayMetrics
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration

private val DarkColorPalette = darkColors(
  primary = Purple200,
  onPrimary = Color.White,

  primaryVariant = Purple700,

  secondary = Teal200,

  background = Color.DarkGray,
  onBackground = Color.White,

  surface = Color.DarkGray,
  onSurface= Color.White,
)

private val LightColorPalette = lightColors(
  primary = Purple700,
  onPrimary = Color.White,

  primaryVariant = Purple700,

  secondary = Teal200,
  onSecondary = Color.Black,

  background = OffWhite,
  onBackground = Color.Black,

  surface = OffWhiteSurface,
  onSurface = Color.Black,
)

@Composable
fun HowLongAgoTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
  val colors = if (darkTheme) {
    DarkColorPalette
  } else {
    LightColorPalette
  }
  val densityDpi = LocalConfiguration.current.densityDpi
  val typography = if(densityDpi < DisplayMetrics.DENSITY_XHIGH) {
    TypographySmall
  } else {
    TypographyNormal
  }
  CompositionLocalProvider(
    LocalContentColor provides colors.onBackground,
  ) {
    MaterialTheme(
      colors = colors,
      typography = typography,
      shapes = Shapes,
      content = content
    )
  }
}