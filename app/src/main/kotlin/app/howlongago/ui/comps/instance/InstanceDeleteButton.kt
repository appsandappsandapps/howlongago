package app.howlongago.ui.comps.instance

import androidx.compose.foundation.clickable
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier

// Delete instance button
// and confirmation popup
@Composable
fun InstanceDeleteButton(
    onDel: () -> Unit,
) {
    var showDeleteAlert by remember { mutableStateOf(false) }
    if (showDeleteAlert)
        AlertDialog(
            { showDeleteAlert = false },
            confirmButton = {
                Button({
                    showDeleteAlert = false
                    onDel()
                }) {
                    Text("Delete it")
                }
            },
            dismissButton = {
                Button({ showDeleteAlert = false }) {
                    Text("Keep it")
                }
            },
            title = { Text("Delete the instance?") },
            text = { Text("Delete the instance, forever?") },
        )
    Icon(
        Icons.Default.Delete, "delete",
        Modifier.clickable { showDeleteAlert = true }
    )
}
