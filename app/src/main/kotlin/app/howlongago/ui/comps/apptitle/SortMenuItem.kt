package app.howlongago.ui.comps.settings

import androidx.compose.foundation.background
import androidx.compose.material.DropdownMenuItem
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.onSurface

// Shows a sort option
// and highlights it if it's
// the current sort
@Composable
fun SortMenuItem(
  currentSort: SortEnum,
  sort: SortEnum,
  itemClick: (SortEnum) -> Unit,
  content: @Composable (Boolean) -> Unit,
) {
  DropdownMenuItem(
    { itemClick(sort) },
    if (currentSort == sort)
      Modifier.background(onSurface.copy(alpha = 0.7f))
    else Modifier
  ) {
    content(currentSort == sort)
  }
}