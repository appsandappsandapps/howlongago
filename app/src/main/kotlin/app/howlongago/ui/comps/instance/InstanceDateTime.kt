package app.howlongago.ui.comps.instance

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.db.Instance
import app.howlongago.onSurface
import app.howlongago.setDateWithCalendar
import app.howlongago.taskdetails.model.InstanceUI
import app.howlongago.ui.comps.AndroidDateView
import app.howlongago.ui.comps.AndroidTimeView
import java.text.SimpleDateFormat
import java.util.*

// Show the date and time
// and if we're in editing mode
// show some dialogues with date
// and time views
@Composable
fun InstanceDateTime(
    modifier: Modifier,
    instance: InstanceUI,
    edit: Boolean = false,
    onUpdate: (Date) -> Unit,
) {
    var showDate by remember { mutableStateOf(false) }
    var showTime by remember { mutableStateOf(false) }
    var date = remember { mutableStateOf(instance.date) }
    if(showDate) {
        AndroidDateView(date.value) {
            it?.let {
                Calendar
                    .getInstance()
                    .apply { time = Date(date.value) }
                    .let { currentTime ->
                        it.setDateWithCalendar(
                            hour = currentTime.get(Calendar.HOUR_OF_DAY),
                            minute = currentTime.get(Calendar.MINUTE)
                        )
                    }
                date.value = it.time
                onUpdate(Date(date.value))
            }
            showDate = false
        }
    }
    if(showTime) {
        AndroidTimeView(Date(date.value)) {
            it?.let {
                date.value = it.time
                onUpdate(Date(date.value))
            }
            showTime = false
        }
    }
    var dateModifier: Modifier = Modifier
    var timeModifier: Modifier = Modifier
    if(edit) {
        dateModifier = dateModifier.clickable { showDate = true }
        timeModifier = timeModifier.clickable { showTime = true }
    }
    Row(
        modifier
    ) {
        Text(
            instance.friendlyDate + if(!edit) "," else " ",
            dateModifier.padding(end = 5.dp),
            color = onSurface.copy(alpha = 0.6f),
            textDecoration =
                if(edit) TextDecoration.Underline
                else TextDecoration.None,
            fontSize = 14.sp,
        )
        Text(
            instance.friendlyTime,
            timeModifier,
            color = onSurface.copy(alpha = 0.6f),
            textDecoration =
                if(edit) TextDecoration.Underline
                else TextDecoration.None,
            fontSize = 14.sp,
        )
    }
}
