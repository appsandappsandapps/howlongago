package app.howlongago.ui.comps.tasklist

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.clickableNoRipple
import app.howlongago.tasklist.model.TaskWithInstanceDateUI
import app.howlongago.ui.comps.LocalNavigationUIEvents
import app.howlongago.ui.comps.LocalTaskListUIState
import app.howlongago.ui.comps.task.TaskRowDateTextStyle
import app.howlongago.ui.comps.task.TaskRowTextStyle

@Composable
fun TaskList(
  modifier: Modifier,
  tasks: List<TaskWithInstanceDateUI> = LocalTaskListUIState.current.tasks,
  onGotoDetail: (Long) -> Unit = LocalNavigationUIEvents.current.let {
    it::onOpenDetailPage
  }
) {
  if (tasks.isEmpty())
    Column(
      modifier.fillMaxSize(),
      verticalArrangement = Arrangement.Center,
      horizontalAlignment = Alignment.CenterHorizontally
    ) {
      Text("No tasks yet! 😯", fontSize = 40.sp)
    }
  else
    LazyColumn(modifier) {
      items(tasks) { task ->
        Surface(
          Modifier
            .padding(top = 2.dp, bottom = 12.dp)
            .clickableNoRipple { onGotoDetail(task.uid) }
            .fillMaxWidth(),
          elevation = 1.dp
        ) {
          Column(Modifier.padding(14.dp)) {
            Text(
              task.taskName!!,
              style = TaskRowTextStyle,
            )
            Text(
              task.friendlyDate,
              style = TaskRowDateTextStyle,
            )
          }
        }
      }
    }
}