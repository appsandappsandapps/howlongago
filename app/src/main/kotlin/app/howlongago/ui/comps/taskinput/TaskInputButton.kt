package app.howlongago.ui.comps

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInRoot
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.unit.dp
import app.howlongago.*
import app.howlongago.ui.comps.task.TaskInputField
import app.howlongago.ui.theme.LeftButtonShape
import app.howlongago.ui.theme.RightButtonShape

// Adds a new tasks to the
// view model and thus database
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TaskInputButton(
  modifier: Modifier = Modifier,
  onSetExpand: () -> Unit = LocalTaskListUIEvents.current.let { it::onToggleExpandTextEntry },
  onYPos: (Float) -> Unit = LocalTaskListUIEvents.current.let { it::ypos },
) {
  BoxWithConstraints {
    Surface(
      modifier = modifier
        .onGloballyPositioned {
          onYPos(it.positionInRoot().y + it.size.height)
        }
        .clickableNoRipple { onSetExpand() },
      elevation = 4.dp,
      shape = RoundedCornerShape(50),
    ) {
      Icon(
        Icons.Filled.Add,
        "add",
        Modifier
          .size(28.dp),
      )
    }
  }
}

