package app.howlongago.ui.comps.settings

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.ComposeFn
import app.howlongago.onBackground
import app.howlongago.ui.comps.task.SettingsAbout
import app.howlongago.ui.comps.task.SettingsStoreLinks
import app.howlongago.versionName

// The about box popup from
// the app menu
@Composable
fun About(
  modifier: Modifier = Modifier,
  close: ComposeFn,
) {
  val context: Context = LocalContext.current
  val version = context.versionName // an extension, see utils.kt
  val uriHandler = LocalUriHandler.current
  Column(modifier) {
    Row(
      verticalAlignment = Alignment.CenterVertically
    ) {
      Text(
        "I made this",
        Modifier.padding(bottom = 6.dp),
        fontSize = 35.sp,
      )
      Spacer(Modifier.weight(1f))
      close(Modifier)
    }
    Divider()
    Text(
      "Version: $version",
      Modifier.padding(top = 8.dp, bottom = 6.dp),
      style = SettingsAbout,
      color = onBackground.copy(alpha = 0.7f),
    )
    Row {
      Text(
        "→  Source code",
        Modifier
          .clickable {
            uriHandler.openUri("https://gitlab.com/appsandappsandapps/howlongago")
          }
          .padding(end = 10.dp),
        style = SettingsStoreLinks,
        color = onBackground.copy(alpha = 0.7f),
      )
      Text(
        "→  Play store",
        Modifier
          .clickable {
            uriHandler.openUri("https://play.google.com/store/apps/details?id=app.howlongago")
          },
        style = SettingsStoreLinks,
        color = onBackground.copy(alpha = 0.7f),
      )
    }
  }
}