package app.howlongago.ui.comps

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import app.howlongago.LocalAppCompatActivity
import app.howlongago.showDatePicker
import java.util.*

// Gets the current date in the calendar
// And calls a callback when it's been updated
@Composable
fun AndroidDateView(
  date: Long,
  onEdit: (Date?) -> Unit,
) {
  val appCompatActivity = LocalAppCompatActivity.current
  LaunchedEffect(Unit) {
    appCompatActivity.showDatePicker(
      Date(date),
      onSelect = {
        onEdit(it)
      },
      onDismiss = {
        onEdit(null)
      },
    )
  }
}