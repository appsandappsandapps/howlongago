package app.howlongago.ui.comps

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import app.howlongago.LocalAppCompatActivity
import app.howlongago.getHourAndMinute
import app.howlongago.setDateWithCalendar
import app.howlongago.showTimePicker
import java.util.*

// Gets the current time in the time picker
// And calls a callback when it's been updated
@Composable
fun AndroidTimeView(
  date: Date,
  onEdit: (Date?) -> Unit,
) {
  val appCompatActivity = LocalAppCompatActivity.current
  LaunchedEffect(Unit) {
    appCompatActivity.showTimePicker(
      date.getHourAndMinute(),
      onSelect = {
        date.setDateWithCalendar(
          hour = it.first,
          minute = it.second,
        )
        onEdit(date)
      },
      onDismiss = {
        onEdit(null)
      }
    )
  }
}