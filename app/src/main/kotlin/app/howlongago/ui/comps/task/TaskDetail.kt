package app.howlongago.ui.comps

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import app.howlongago.*
import app.howlongago.taskdetails.model.InstanceUI
import app.howlongago.ui.comps.instance.InstanceRowExpandable
import app.howlongago.ui.comps.task.InstanceInbetweenTextStyle
import app.howlongago.ui.comps.task.TaskDetailTopBar

// Editable title
// Add instance button
// List of instances
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TaskDetail(
  taskId: Long,
  modifier: Modifier = Modifier,
  instances: List<InstanceUI> = LocalTaskDetailsUIState.current.instances,
) {
  Column(
    modifier
      .background(background)
      .padding(all = 14.dp)
      .padding(top = 20.dp),
  ) {
    TaskDetailTopBar(
      Modifier.requiredHeightIn(20.dp, 200.dp),
      taskId,
    )
    LazyColumn(
      Modifier
        .weight(2f)
    ) {
      items(instances) { instance ->
        key(instance.uid) {
          Column {
            Surface(
              Modifier
                .padding(bottom = 10.dp)
                .padding(top = if(instances.indexOf(instance) == 0) 30.dp else 2.dp),
              elevation = 1.dp
            ) {
              InstanceRowExpandable(
                instance = instance,
                expandToEdit = instance.expanded,
              )
            }
            // Show the inbetween text if not the last item
            if(instances.indexOf(instance) != instances.size - 1)
              Row(
                Modifier.padding(bottom = 10.dp),
                verticalAlignment = Alignment.CenterVertically,
              ) {
                Spacer(Modifier.weight(2f))
                Text(
                  instance.friendlyDateInbetween,
                  Modifier.padding(end = 2.dp),
                  style = InstanceInbetweenTextStyle
                )
              }
          }
        }
      }
    }
  }
}