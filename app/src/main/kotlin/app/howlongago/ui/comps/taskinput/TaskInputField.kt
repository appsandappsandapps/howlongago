package app.howlongago.ui.comps.task

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.KeyboardOptionsDone
import app.howlongago.ui.comps.LocalTaskListUIEvents
import app.howlongago.ui.comps.LocalTaskListUIState
import app.howlongago.ui.theme.LeftButtonShape
import app.howlongago.ui.theme.RightButtonShape
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

// The input text in the expanded
// text entry with placeholder text
// and cancel and add buttons
@OptIn(
  ExperimentalAnimationApi::class,
  ExperimentalComposeUiApi::class,
)
@Composable
fun TaskInputField(
  modifier: Modifier = Modifier,
  setText: (String) -> Unit = LocalTaskListUIEvents.current.let { it::onSetNewTaskInputText },
  onTextFinished: () -> Unit = LocalTaskListUIEvents.current.let { it::onNewEntryInsertDelay },
  onExpandToggle: () -> Unit = LocalTaskListUIEvents.current.let { it::onToggleExpandTextEntry },
  expanded: Boolean = LocalTaskListUIState.current.expandTextEntry,
  inputText: String = LocalTaskListUIState.current.newTaskInputText,
) {
  val scope = rememberCoroutineScope()
  val keyboardController = LocalSoftwareKeyboardController.current
  val focusManager = LocalFocusManager.current
  val focusRequester by remember { mutableStateOf(FocusRequester()) }
  var rememberExpanded by remember { mutableStateOf(false) }
  if(expanded) {
    BackHandler { onExpandToggle() }
    scope.launch {
      delay(700)
      focusRequester.requestFocus()
    }
    rememberExpanded = true
  } else {
    if(rememberExpanded) {
      scope.launch {
        delay(700)
        focusManager.clearFocus()
        keyboardController?.hide()
      }
      rememberExpanded = false
    }
  }
  Box {
    BasicTextField(
      inputText,
      { setText(it) },
      modifier
        .padding(10.dp)
        .background(Color.Transparent)
        .fillMaxWidth()
        .focusRequester(focusRequester),
      cursorBrush = SolidColor(Color.White),
      textStyle = TaskInputFontStyle,
      keyboardOptions = KeyboardOptionsDone,
      keyboardActions = KeyboardActions {
        if (inputText.isNotEmpty()) {
          keyboardController?.hide()
          onTextFinished()
        }
      },
      decorationBox = {
        Box(Modifier.padding(4.dp)) {
          if (inputText.trim().isEmpty())
            Text(
              "e.g. \uD83C\uDF3B︎ bought flowers, \uD83D\uDC08 fed the lion",
              Modifier.alpha(0.4f),
              fontSize = 30.sp
            )
          it()
        }
      },
    )
    Row(Modifier.align(Alignment.BottomStart)) {
        Spacer(Modifier.weight(1f))
        Button({
            scope.launch {
              delay(700)
              keyboardController?.hide()
            }
            onTextFinished()
          },
          shape = LeftButtonShape
        ) {
          Icon(Icons.Filled.Create, "add")
        }
      }
  }
}