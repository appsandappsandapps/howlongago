package app.howlongago.ui.comps.task

import androidx.compose.foundation.clickable
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import app.howlongago.ui.comps.LocalNavigationUIEvents
import app.howlongago.ui.comps.LocalTaskDetailsUIEvents

// A delete icon with a
// confirming alert dialog
@Composable
fun TaskDeleteButton(
  taskId: Long,
  goBack: () -> Unit = LocalNavigationUIEvents.current.let {
    it::goBackoListPage
  },
  deleteTask: (Long) -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onDeleteTask
  },
) {
  var showDeleteAlert by remember { mutableStateOf(false) }
  val deleteIt: @Composable () -> Unit = {
    Button({
      showDeleteAlert = false
      deleteTask(taskId)
      goBack()
    }) {
      Text("Delete it")
    }
  }
  val keepIt: @Composable () -> Unit = {
    Button({ showDeleteAlert = false }) {
      Text("Keep it")
    }
  }
  if(showDeleteAlert)
    AlertDialog(
      { showDeleteAlert = false },
      confirmButton = { deleteIt() },
      dismissButton = { keepIt() },
      title = { Text("Delete it?") },
      text = { Text("Delete the task and all its instances, forever?") },
    )
  Icon(
    Icons.Default.Delete,
    "delete",
    Modifier.clickable {
      showDeleteAlert = true
    }
  )
}