package app.howlongago.ui.comps.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import app.howlongago.Preferences
import app.howlongago.onBackground
import app.howlongago.ui.comps.task.SettingsStoreLinks
import app.howlongago.ui.comps.task.SettingsTheme

// Changes light/dark mode
// and updates the preferences
// accordingly
@Composable
fun Theme(
  modifier: Modifier = Modifier,
) {
  val darkMode = Preferences.darkTheme.collectAsState()
  val setDarkMode = { b: Boolean ->
    Preferences.setDarkTheme(b)
  }
  Column(modifier) {
    Row(
      verticalAlignment = Alignment.CenterVertically
    ){
      Text(
        "Super lovely dark mode",
        style = SettingsTheme,
        color = onBackground.copy(alpha = 0.7f),
      )
      Switch(
        checked = darkMode.value,
        { setDarkMode(it) },
        colors = SwitchDefaults.colors(
          checkedThumbColor = MaterialTheme.colors.primary
        )
      )
    }
  }
}