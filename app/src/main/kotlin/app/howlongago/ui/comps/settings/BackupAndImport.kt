package app.howlongago.ui.comps.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.LocalAppCompatActivity
import app.howlongago.background
import app.howlongago.launchDatabaseBackupActivity
import app.howlongago.launchDatabaseImportActivity
import app.howlongago.onBackground
import app.howlongago.ui.comps.task.SettingsBackupImportPointerTextStyle
import app.howlongago.ui.comps.task.SettingsBackupImportTextStyle
import app.howlongago.ui.comps.task.SettingsBackupPointerTextStyle

@Composable
fun BackupAndImport (
  modifier: Modifier = Modifier,
) {
  val activity = LocalAppCompatActivity.current
  Column(modifier) {
    Button(
      { launchDatabaseBackupActivity(activity) },
      Modifier.padding(top = 10.dp)
    ) {
      Text(
        "Backup to filesystem",
        color = background,
      )
    }
    Button(
      { launchDatabaseImportActivity(activity) },
      Modifier.padding(top = 0.dp)
    ) {
      Text(
        "Import new tasks",
        color = background,
      )
    }
    BackupImportNoteTextItem(
      "You can backup to and import from google drive etc",
    )
    BackupImportNoteTextItem(
      "Importing doesn't remove your existing tasks",
    )
  }
}

@Composable
private fun BackupImportNoteTextItem(text: String) {
  Row(
    Modifier.padding(top = 4.dp),
    verticalAlignment = Alignment.Top
  ) {
    Text(
      "☞  ",
      style = SettingsBackupImportPointerTextStyle,
    )
    Text(
      text,
      style = SettingsBackupImportTextStyle,
    )
  }
}