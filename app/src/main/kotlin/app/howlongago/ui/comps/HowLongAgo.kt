package app.howlongago.ui.comps

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSavedStateRegistryOwner
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import app.howlongago.LifecycleOnResumeObserver
import app.howlongago.StateSaver
import app.howlongago.background
import app.howlongago.navigation.NavigationUIEvents
import app.howlongago.navigation.NavigationUIState
import app.howlongago.navigation.NavigationViewModel
import app.howlongago.taskdetails.TaskDetailsUIEvents
import app.howlongago.taskdetails.TaskDetailsUIState
import app.howlongago.taskdetails.TaskDetailsViewModel
import app.howlongago.tasklist.TasksUIEvents
import app.howlongago.tasklist.TasksUIState
import app.howlongago.tasklist.TasksViewModel
import app.howlongago.ui.comps.settings.SettingsScreen
import kotlinx.coroutines.flow.MutableStateFlow
import kotlin.Suppress

val LocalNavigationUIState = compositionLocalOf { NavigationUIState() }
val LocalNavigationUIEvents = compositionLocalOf {
  NavigationUIEvents(NavigationViewModel(), MutableStateFlow(NavigationUIState()))
}

val LocalTaskListUIState = compositionLocalOf { TasksUIState() }
val LocalTaskListUIEvents = compositionLocalOf { TasksUIEvents() }

val LocalTaskDetailsUIState = compositionLocalOf { TaskDetailsUIState() }
val LocalTaskDetailsUIEvents = compositionLocalOf { TaskDetailsUIEvents() }

/**
 * - Creates the list, detail and settings screen
 */
// TODO: Readd going directly to task with taskId
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun HowLongAgo() {

  // Data for the navigation ui model
  val stateOwner = LocalSavedStateRegistryOwner.current
  @Suppress("UNCHECKED_CAST")
  val navigationUIModel = viewModel<NavigationViewModel>(factory = object : AbstractSavedStateViewModelFactory(stateOwner, null) {
    override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T =
      NavigationViewModel(StateSaver(handle)) as T
  }).navigationUIModel
  val navigationUIState by navigationUIModel.uiState.collectAsState()
  val navigationUIEvents = navigationUIModel.uiEvents

  @Suppress("UNCHECKED_CAST")
  val tasksUIModel = viewModel<TasksViewModel>(factory = object :
    AbstractSavedStateViewModelFactory(stateOwner, null) {
    override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T =
      TasksViewModel(StateSaver(handle)) as T
  }).tasksUIModel
  val tasksUIState by tasksUIModel.uiState.collectAsState()
  val tasksUIEvents = tasksUIModel.uiEvents

  @Suppress("UNCHECKED_CAST")
  val taskDetailsUIModel = viewModel<TaskDetailsViewModel>(factory = object : AbstractSavedStateViewModelFactory(stateOwner, null) {
    override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T =
      TaskDetailsViewModel(StateSaver<TaskDetailsUIState>(handle)) as T
  }).taskDetailsUIModel
  val taskDetailsUIState by taskDetailsUIModel.uiState.collectAsState()
  val taskDetailsUIEvents = taskDetailsUIModel.uiEvents
  LifecycleOnResumeObserver { taskDetailsUIEvents.onForceUIUpdate() }

  CompositionLocalProvider(
    LocalNavigationUIState provides navigationUIState,
    LocalNavigationUIEvents provides navigationUIEvents,
    LocalTaskListUIState provides tasksUIState,
    LocalTaskListUIEvents provides tasksUIEvents,
    LocalTaskDetailsUIState provides taskDetailsUIState,
    LocalTaskDetailsUIEvents provides taskDetailsUIEvents,
  ) {
    Box(
      Modifier
        .fillMaxSize()
        .background(background)
    ) {
      TaskListScreen()
      TaskDetailScreen()
      SettingsScreen()
    }
  }

}