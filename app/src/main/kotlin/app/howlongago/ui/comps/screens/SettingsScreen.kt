package app.howlongago.ui.comps.settings

import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import app.howlongago.background
import app.howlongago.ui.comps.LocalNavigationUIEvents
import app.howlongago.ui.comps.LocalNavigationUIState

@Composable
fun SettingsScreen(
  toggledSettings: Boolean = LocalNavigationUIState.current.toggledSettings,
  onCloseSettings: () -> Unit = LocalNavigationUIEvents.current.let { it::onToggleSettings },
) {
  BoxWithConstraints {
    if(toggledSettings) BackHandler {
      onCloseSettings()
    }
    // Animate it into place
    val settingsTranslationX by animateFloatAsState(
      if (toggledSettings) 0f
      else with(LocalDensity.current) { maxWidth.toPx() },
      animationSpec = spring(
        dampingRatio = Spring.DampingRatioLowBouncy,
        stiffness = Spring.StiffnessLow
      )
    )
    Box(
        Modifier.graphicsLayer {
          translationX = settingsTranslationX
        }
        .fillMaxSize()
        .padding(5.dp)
        .background(background, RoundedCornerShape(5.dp))
        .border(3.dp, Color.Gray, RoundedCornerShape(5.dp))
        .clickable { }
    ) {
      Column(
        Modifier
          .padding(top = 5.dp, bottom = 0.dp, start = 15.dp, end = 10.dp)
      ) {
        About(
          Modifier,
          close = {
            Icon(
              Icons.Filled.Close,
              "close",
              Modifier
                .clickable { onCloseSettings() },
              tint = Color.Gray
            )
          }
        )
        Divider(Modifier.padding(top = 10.dp))
        Theme()
        Divider()
        BackupAndImport()
      }
    }
  }
}