package app.howlongago

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.ui.graphics.vector.VectorProperty
import app.howlongago.usecases.BackupImportUseCase
import app.howlongago.usecases.repositories.TasksRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.lang.RuntimeException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import java.time.LocalDateTime

// TODO: Make a better UI reponse than toast
fun saveDatabaseWithUI(
  data: Uri,
  backupImportUseCase: BackupImportUseCase = ServiceLocator.backupImportUseCase!!,
) = MainScope().launch(Dispatchers.Main) {
  // Get the output stream
  val appContext = Application.instance!!.applicationContext
  val outputStream = appContext.contentResolver.openOutputStream(data)
  // Save the export to the outputstream
  if (outputStream != null) {
    exportDatabaseToOutputStream(
      backupImportUseCase.exportDatabase(),
      outputStream,
    )
    // Show the UI
    Toast.makeText(appContext, "Database saved to your file system", Toast.LENGTH_LONG).show()
  } else {
    Toast.makeText(appContext, "Error selecting backup file", Toast.LENGTH_LONG).show()
  }
}

// TODO: Make a better UI reponse than toast
fun importDatabaseWithUI(
  data: Uri,
  backupImportUseCase: BackupImportUseCase = ServiceLocator.backupImportUseCase!!,
) = MainScope().launch(UIDispatcher) {
  // Get the input stream
  val appContext = Application.instance!!.applicationContext
  val inputStream = appContext.contentResolver.openInputStream(data)
  // Save the database from the input stream
  try {
    if (inputStream != null) {
      val db = importDatabaseFromInputStream(inputStream)
      backupImportUseCase.importDatabase(db)
      Toast.makeText(appContext, "Imported tasks", Toast.LENGTH_LONG).show()
    } else {
      Toast.makeText(appContext, "Error getting backup file", Toast.LENGTH_LONG).show()
    }
  } catch (e: Exception) {
    Toast.makeText(appContext, "Error importing: ${e.message}", Toast.LENGTH_LONG).show()
  }
}

fun launchDatabaseBackupActivity(context: Activity) {
  val now = LocalDateTime.now()
  val dateString = with(now) {
    "${year}_${monthValue.toString().padStart(2, '0')}_${dayOfMonth.toString().padStart(2, '0')}_${hour}_${minute}"
  }
  val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
    addCategory(Intent.CATEGORY_OPENABLE)
    type = "plain/text"
    putExtra(Intent.EXTRA_TITLE, "howlongago_db_android_${dateString}.txt")
  }
  context.startActivityForResult(intent, MainActivity.SAVE_DATABASE_REQUEST)
}

fun launchDatabaseImportActivity(context: Activity) {
  val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
    addCategory(Intent.CATEGORY_OPENABLE)
    type = "*/*"
  }
  context.startActivityForResult(intent, MainActivity.IMPORT_DATABASE_REQUEST)
}

private suspend fun importDatabaseFromInputStream(
  inputStream: InputStream
): String {
  val br = BufferedReader(InputStreamReader(inputStream))
  val backup = br.readText()
  br.close()
  return backup
}

private suspend fun exportDatabaseToOutputStream(
  json: String,
  outputStream: OutputStream,
) {
    try {
      val bw = BufferedWriter(OutputStreamWriter(outputStream))
      bw.write("\n")
      bw.write(json)
      bw.flush()
      bw.close()
    } catch (e: Exception) {
      e.printStackTrace()
    }
}
