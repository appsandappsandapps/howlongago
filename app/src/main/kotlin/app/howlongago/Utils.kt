package app.howlongago

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import java.util.*

val Context.versionName: String
  get() = try {
    packageManager.getPackageInfo(packageName, 0).versionName
  } catch (e: Exception) {
    "Couldn't find version name..."
  }

val Context.prefs : SharedPreferences
  get() = getSharedPreferences("howlongago", Context.MODE_PRIVATE)

val Context.editPrefs : SharedPreferences.Editor
  get() = getSharedPreferences("howlongago", Context.MODE_PRIVATE).edit()

// sets the date with years, month etc params
fun Date.setDateWithCalendar(
  year: Int? = null,
  month: Int? = null,
  dom: Int? = null,
  hour: Int? = null,
  minute: Int? = null) {
  val date = this
  time = Calendar.getInstance().apply {
    time = date
    year?.let { set(Calendar.YEAR, it) }
    month?.let { set(Calendar.MONTH, it) }
    dom?.let { set(Calendar.DAY_OF_MONTH, it) }
    hour?.let { set(Calendar.HOUR_OF_DAY, it) }
    minute?.let { set(Calendar.MINUTE, it) }
  }.timeInMillis
}

data class DateTimeAMPM(
  val hour:Int,
  val minute:Int,
)

// returns the hours and minutes from a date
fun Date.getHourAndMinute():DateTimeAMPM {
  val date = this
  return Calendar.getInstance().let {
    it.time = date
    val pm = it.get(Calendar.AM_PM) == Calendar.PM
    return DateTimeAMPM(
      hour = it.get(Calendar.HOUR) + if (pm) 12 else 0,
      minute = it.get(Calendar.MINUTE),
    )
  }
}

fun android.app.Application.instance() = this as Application

// show an android material date picker
fun AppCompatActivity.showDatePicker(
  date:Date = Date(),
  onSelect: (Date) -> Unit,
  onDismiss: () -> Unit,
) {
  MaterialDatePicker.Builder.datePicker()
    .setSelection(date.time)
    .setTheme(R.style.Calendar)
    .build()
    .apply {
      addOnPositiveButtonClickListener { onSelect(Date(it)) }
      addOnDismissListener { onDismiss() }
      show(supportFragmentManager, "tag")
    }
}

// show an android material time picker
fun AppCompatActivity.showTimePicker(
  hourMinute: DateTimeAMPM,
  onSelect: (Pair<Int, Int>) -> Unit,
  onDismiss: () -> Unit,
) {
  MaterialTimePicker.Builder()
    .setHour(hourMinute.hour)
    .setMinute(hourMinute.minute)
    .setTimeFormat(TimeFormat.CLOCK_12H)
    .build()
    .apply {
      addOnPositiveButtonClickListener {
        onSelect(Pair(hour, minute))
      }
      addOnDismissListener { onDismiss() }
      show(supportFragmentManager, "tag")
    }
}

fun Modifier.clickableNoRipple(
  callback: () -> Unit
): Modifier = composed {
  val interactionSource = remember { MutableInteractionSource() }
  this.clickable(
    interactionSource = interactionSource,
    indication = null
  ) { callback() }
}

val KeyboardOptionsDone = KeyboardOptions(
  imeAction = ImeAction.Done
)

val primary: Color @Composable get() = MaterialTheme.colors.primary
val onPrimary: Color @Composable get() = MaterialTheme.colors.onPrimary
val secondary: Color @Composable get() = MaterialTheme.colors.secondary
val onSecondary: Color @Composable get() = MaterialTheme.colors.onSecondary
val surface: Color @Composable get() = MaterialTheme.colors.surface
val onSurface: Color @Composable get() = MaterialTheme.colors.onSurface
val background: Color @Composable get() = MaterialTheme.colors.background
val onBackground: Color @Composable get() = MaterialTheme.colors.onBackground

val h1: TextStyle @Composable get() = MaterialTheme.typography.h1
val h2: TextStyle @Composable get() = MaterialTheme.typography.h2
val h3: TextStyle @Composable get() = MaterialTheme.typography.h3
val h4: TextStyle @Composable get() = MaterialTheme.typography.h4
val button: TextStyle @Composable get() = MaterialTheme.typography.button
val body1: TextStyle @Composable get() = MaterialTheme.typography.body1
val subtitle1: TextStyle @Composable get() = MaterialTheme.typography.subtitle1

typealias ComposeFn = @Composable (Modifier) -> Unit

@Composable
fun LifecycleOnResumeObserver(onResume: () -> Unit) {
  val lifecycleOwner = LocalLifecycleOwner.current
  DisposableEffect(lifecycleOwner) {
    val lifecycle = lifecycleOwner.lifecycle
    val observer = LifecycleEventObserver { _, event ->
      if(event == Lifecycle.Event.ON_RESUME) {
        onResume()
      }
    }
    lifecycle.addObserver(observer)
    onDispose {
      lifecycle.removeObserver(observer)
    }
  }
}

