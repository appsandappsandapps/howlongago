package app.howlongago

import app.howlongago.db.HowlongagoDatabase
import app.howlongago.db.TasksAndInstancesQueries
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver

actual fun tasksDbInit(context: AppContext): TasksAndInstancesQueries {
  val driver = NativeSqliteDriver(
    schema = HowlongagoDatabase.Schema,
    name = "howlongagodb",
  )
  return HowlongagoDatabase(driver).tasksAndInstancesQueries
}
