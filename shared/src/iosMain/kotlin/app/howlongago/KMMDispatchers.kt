package app.howlongago

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Runnable
import platform.darwin.dispatch_async
import platform.darwin.dispatch_get_main_queue
import kotlin.coroutines.CoroutineContext

//actual val UIDispatcher: CoroutineDispatcher = Dispatchers.Main
//actual val UIImmediateDispatcher: CoroutineDispatcher = Dispatchers.Main.immediate
//actual val DefaultDispatcher: CoroutineDispatcher = Dispatchers.Default
actual val IODispatcher: CoroutineDispatcher = Dispatchers.Default
