package app.howlongago

import app.howlongago.db.Instance
import app.howlongago.db.TaskWithInstance

actual annotation class AppParcelize
actual interface AppParcelable

actual open class StateSaver<T : AppParcelable>() : StateSavable<T> {
  override fun get(name: String): T? = null
  override fun set(name: String, state: T) {}
}

