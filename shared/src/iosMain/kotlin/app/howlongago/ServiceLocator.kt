package app.howlongago

import app.howlongago.db.TasksAndInstancesQueries
import app.howlongago.usecases.BackupImportUseCase
import app.howlongago.usecases.TaskDetailsUseCase
import app.howlongago.usecases.TasksSortedUseCase
import app.howlongago.usecases.datasources.TaskSortingDatasource
import app.howlongago.usecases.repositories.InstancesRepository
import app.howlongago.usecases.repositories.TasksRepository
import app.howlongago.usecases.repositories.TasksSortingRepository

actual object ServiceLocator {

  actual var sortedTasksUseCase: TasksSortedUseCase? = null
  actual var backupImportUseCase: BackupImportUseCase? = null
  actual var taskDetailsUseCase: TaskDetailsUseCase? = null

  actual fun init(context: AppContext) {
    val tasksDb: TasksAndInstancesQueries = tasksDbInit(context)
    val taskRepository: TasksRepository = TasksRepository(tasksDb)
    val taskSortingDatasource = TaskSortingDatasource(context)
    val tasksSortingRepo = TasksSortingRepository(taskSortingDatasource)
    val instancesRepo = InstancesRepository(tasksDb)
    sortedTasksUseCase = TasksSortedUseCase(taskRepository, tasksSortingRepo)
    taskDetailsUseCase = TaskDetailsUseCase(taskRepository, instancesRepo, sortedTasksUseCase!!)
    backupImportUseCase = BackupImportUseCase(taskRepository, instancesRepo, sortedTasksUseCase!!)
  }

}