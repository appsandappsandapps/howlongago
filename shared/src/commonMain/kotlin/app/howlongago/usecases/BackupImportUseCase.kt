package app.howlongago.usecases

import app.howlongago.InstanceBackup
import app.howlongago.TaskBackup
import app.howlongago.TaskWithInstancesBackup
import app.howlongago.db.Instance
import app.howlongago.db.Task
import app.howlongago.db.TasksBackup
import app.howlongago.usecases.repositories.InstancesRepository
import app.howlongago.usecases.repositories.TasksRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class BackupImportUseCase(
  private val tasksRepo: TasksRepository,
  private val instancesRepo: InstancesRepository,
  private val tasksSortedUseCase: TasksSortedUseCase,
) {

  @Throws(Exception::class)
  public suspend fun importDatabase(stringDb: String) {
    val db: List<TaskWithInstancesBackup> = Json {
      ignoreUnknownKeys = true
    }.decodeFromString(stringDb)
    db.forEach {
      val task_id = tasksRepo.insert(it.task.taskName)
      it.instances?.forEach {
        instancesRepo.insert(
          task_id,
          it.date,
          it.notes,
        )
      }
    }
    tasksSortedUseCase.getAllSortedIntoMutableState()
  }

  public suspend fun exportDatabase(): String {
    return tasksRepo.tasksBackup()
      .groupBy { it.uid }.values
      .map {
        TaskWithInstancesBackup(
          TaskBackup(it[0].uid, it[0].task_name),
          it
            .filter { it.task_id != null }
            .mapNotNull {
              InstanceBackup(
                it.instance_uid!!,
                it.task_id!!,
                it.date!!,
                it.notes!!,
                it.nextDate
              )
            }
        )
      }.let {
        Json {
          ignoreUnknownKeys = true
        }.encodeToString(it)
      }
  }

}