package app.howlongago.usecases.datasources

import app.howlongago.AppContext
import com.russhwolf.settings.Settings

enum class SortEnum { NAME_ASC, NAME_DESC, DATE_ASC, DATE_DESC }

class TaskSortingDatasource(
  val context: AppContext
) {

  val settings: Settings = Settings()

  fun setSort(sort: SortEnum) {
    settings.putString("sortEnum", sort.toString())
  }

  fun getSort(): SortEnum {
    return settings.getString("sortEnum", SortEnum.DATE_DESC.toString())
      .let { SortEnum.valueOf(it) }
  }

}