package app.howlongago.usecases

import app.howlongago.db.Instance
import app.howlongago.db.Task
import app.howlongago.usecases.repositories.InstancesRepository
import app.howlongago.usecases.repositories.TasksRepository
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * - Provides and updates the task and instance details
 * - Updates the task list when instance details update
 */
class TaskDetailsUseCase(
  private val tasksRepo: TasksRepository,
  private val instancesRepo: InstancesRepository,
  private val sortedTaskUseCase: TasksSortedUseCase,
) {

  var selectedTask: MutableStateFlow<Task> = MutableStateFlow(Task(0, ""))
  var instances: MutableStateFlow<List<Instance>> = MutableStateFlow(listOf())

  public suspend fun setSelectedTask(taskId: Long) {
    selectedTask.value = tasksRepo.get(taskId)
    instances.value = instancesRepo.getAll(taskId)
  }

  public suspend fun deleteTask(taskId: Long) {
    tasksRepo.deleteWithInstances(taskId)
    sortedTaskUseCase.getAllSortedIntoMutableState()
  }

  public suspend fun updateTaskName(uid: Long, name: String) {
    tasksRepo.updateNameDelayed(uid, name) {
      sortedTaskUseCase.getAllSortedIntoMutableState()
    }
  }

  public suspend fun insertInstance(taskId: Long): Long {
    val instanceId = instancesRepo.insert(taskId)
    instances.value = instancesRepo.getAll(taskId)
    sortedTaskUseCase.getAllSortedIntoMutableState()
    return instanceId
  }

  public suspend fun deleteInstance(taskId: Long, instanceId: Long) {
    instancesRepo.delete(instanceId)
    instances.value = instancesRepo.getAll(taskId)
    sortedTaskUseCase.getAllSortedIntoMutableState()
  }

  public suspend fun updateInstanceNote(taskId: Long, instanceId: Long, note: String) {
    instancesRepo.updateNoteDelayed(instanceId, note) {
      instances.value = instancesRepo.getAll(taskId)
    }
  }

  public suspend fun updateInstanceDate(taskId: Long, instanceId: Long, date: Long) {
    instancesRepo.updateDate(instanceId, date)
    instances.value = instancesRepo.getAll(taskId)
    sortedTaskUseCase.getAllSortedIntoMutableState()
  }

}