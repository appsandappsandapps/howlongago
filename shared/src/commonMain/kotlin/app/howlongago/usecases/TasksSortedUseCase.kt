package app.howlongago.usecases

import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.db.TaskWithInstance
import app.howlongago.usecases.repositories.TasksRepository
import app.howlongago.usecases.repositories.TasksSortingRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * - List the sorted tasks
 * - Reflects and changes the sort
 * - Inserts a new task
 */
class TasksSortedUseCase(
  private val tasksRepo: TasksRepository,
  private val sortingRepo: TasksSortingRepository,
) {

  var tasks: MutableStateFlow<List<TaskWithInstance>> = MutableStateFlow(listOf())
  var selectedSort: MutableStateFlow<SortEnum> = sortingRepo.selectedSort

  public suspend fun taskInsert(taskName: String): Long {
    val taskId = tasksRepo.insert(taskName)
    getAllSortedIntoMutableState()
    return taskId
  }

  public suspend fun getAllSortedIntoMutableState() {
    tasks.value = getAllSorted()
  }

  private suspend fun getAllSorted(): List<TaskWithInstance> {
    var sort = sortingRepo.selectedSort.value
    var sortedTasks = when (sort) {
      SortEnum.DATE_DESC -> tasksRepo.getAllDateDesc()
      SortEnum.DATE_ASC -> tasksRepo.getAllDateAsc()
      SortEnum.NAME_DESC -> tasksRepo.getAllNameDesc()
      SortEnum.NAME_ASC -> tasksRepo.getAllNameAsc()
    }
    return if(sort == SortEnum.DATE_DESC || sort == SortEnum.DATE_ASC) {
      sortedTasks.sortedWith { a, b ->
        if (a.date == null && b.date == null) {
          if (a.uid > b.uid) -1 else 1
        } else if (a.date == null && b.date != null) {
          -1
        } else if (b.date == null && a.date != null) {
          1
        } else {
          0
        }
      }
    } else {
      sortedTasks
    }
  }

  public suspend fun setTaskSort(sort: SortEnum) {
    sortingRepo.setTaskSort(sort)
    getAllSortedIntoMutableState()
  }

}