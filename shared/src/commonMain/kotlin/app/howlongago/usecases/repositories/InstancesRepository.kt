package app.howlongago.usecases.repositories

import app.howlongago.IODispatcher
import app.howlongago.db.Instance
import app.howlongago.db.TasksAndInstancesQueries
import app.howlongago.delayLong
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock

/**
 * - Modifies instances
 * - Delayed update methods for instance notes update
 */
class InstancesRepository(
  private val db: TasksAndInstancesQueries,
  private val ioScope: CoroutineScope = CoroutineScope(IODispatcher),
  private var updateInstanceNoteDelayedJob: Job? = null
) {

  public suspend fun getAll(taskId: Long): List<Instance> {
    return db.getInstances(taskId).executeAsList()
  }

  public suspend fun insert(
    taskId: Long,
    unixTime: Long = Clock.System.now().toEpochMilliseconds()
  ): Long {
    return insert(
      taskId = taskId,
      date = unixTime,
      note = "",
    )
  }

  public suspend fun insert(
    taskId: Long,
    date: Long,
    note: String
  ): Long {
    return db.transactionWithResult {
      db.insertInstance(
        uid = null,
        task_id = taskId,
        date = date,
        notes = note,
        nextDate = null,
      )
      db.latestInstance().executeAsOne()
    }
  }

  public suspend fun updateNoteDelayed(
    uid: Long,
    note: String,
    onComplete: suspend () -> Unit
  ) {
    updateInstanceNoteDelayedJob?.cancel()
    updateInstanceNoteDelayedJob = ioScope.launch {
      delayLong(400)
      val instance = db.getInstance(uid).executeAsOne().copy(notes = note)
      update(instance)
      onComplete()
    }
  }

  public suspend fun updateDate(
    instanceId: Long,
    date: Long
  ) {
    val instance: Instance = db.getInstance(instanceId).executeAsOne()
    update(instance.copy(date = date))
  }

  private suspend fun update(instance: Instance) {
    db.updateInstance(
      date = instance.date,
      notes = instance.notes,
      nextDate = instance.nextDate,
      uid = instance.uid,
    )
  }

  public suspend fun delete(instanceId: Long) {
    db.deleteInstance(instanceId)
  }

}