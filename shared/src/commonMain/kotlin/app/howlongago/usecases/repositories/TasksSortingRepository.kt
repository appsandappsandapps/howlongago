package app.howlongago.usecases.repositories

import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.usecases.datasources.TaskSortingDatasource
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * - Sets the task sort
 * - Reflects the updated sort
 */
class TasksSortingRepository(
  private val taskSortingDatasource: TaskSortingDatasource,
) {

  var selectedSort: MutableStateFlow<SortEnum> = MutableStateFlow(taskSortingDatasource.getSort())

  fun setTaskSort(sort: SortEnum) {
    selectedSort.value = sort
    taskSortingDatasource.setSort(sort)
  }

}