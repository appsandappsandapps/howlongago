package app.howlongago.navigation

import app.howlongago.AppParcelable
import app.howlongago.AppParcelize
import app.howlongago.taskdetails.TaskDetailsUIState
import app.howlongago.taskdetails.TaskDetailsViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@OptIn(ExperimentalMultiplatform::class)
@AppParcelize
data class NavigationUIState(
  val toggledSettings: Boolean = false,
  val page: String = "list",
  val taskIdForDetailsPage: Long = -1,
) : AppParcelable {
  constructor(foriOSConstructor: Boolean) : this() // ios default constructor
}

class NavigationUIEvents (
  private val navigationViewModel: NavigationViewModel,
  private val uiState: MutableStateFlow<NavigationUIState>,
) {

  constructor() : this(NavigationViewModel(), MutableStateFlow(NavigationUIState())) // for stub

  fun onToggleSettings() {
    uiState.value = uiState.value.copy(
      toggledSettings = !uiState.value.toggledSettings
    )
  }

  fun onOpenDetailPage(taskId: Long) {
    navigationViewModel
      .loadTaskDetails(taskId)
      .invokeOnCompletion {
        uiState.value = uiState.value.copy(
          taskIdForDetailsPage = taskId,
          page = "detail"
        )
      }
  }

  fun goBackoListPage() {
    uiState.value = uiState.value.copy(
      page = "list"
    )
  }

}

class NavigationUIModel(
  navigationViewModel: NavigationViewModel,
  existingState: NavigationUIState,
) {

  public val uiState = MutableStateFlow(existingState)
  public val uiEvents = NavigationUIEvents(navigationViewModel, uiState)

}