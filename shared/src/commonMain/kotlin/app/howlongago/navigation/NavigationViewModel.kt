package app.howlongago.navigation

import app.howlongago.*
import app.howlongago.usecases.TaskDetailsUseCase
import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.debounce

/**
 * For the task detail page
 */
open class NavigationViewModel(
  private val savedState: StateSavable<NavigationUIState>,
  private val taskDetails: TaskDetailsUseCase = ServiceLocator.taskDetailsUseCase!!,
  private val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + UIImmediateDispatcher),
  private val ioDispatcher: CoroutineDispatcher = IODispatcher,
) : AppViewModel() {

  constructor() : this(EmptyStateSaver<NavigationUIState>())

  val navigationUIModel = NavigationUIModel(
    this,
    savedState.get("uiState") ?: NavigationUIState(),
  )

  init {
    coroutineScope.launch(ioDispatcher) {
      navigationUIModel.uiState
        .debounce(500)
        .collect { savedState.set("uiState", it) }
    }
  }

  fun loadTaskDetails(taskId: Long): Job = coroutineScope.launch(ioDispatcher) {
    taskDetails.setSelectedTask(taskId)
  }

}
