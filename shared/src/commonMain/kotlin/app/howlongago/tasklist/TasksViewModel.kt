package app.howlongago.tasklist

import app.howlongago.*
import kotlinx.coroutines.launch
import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.usecases.TaskDetailsUseCase
import app.howlongago.usecases.TasksSortedUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.debounce

/**
 * Diplays the tasks on the main screen
 */
open class TasksViewModel(
  private val savedState: StateSavable<TasksUIState>,
  private val tasksSortedUseCase: TasksSortedUseCase = ServiceLocator.sortedTasksUseCase!!,
  private val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + UIImmediateDispatcher)
) : AppViewModel() {

  constructor() : this(EmptyStateSaver<TasksUIState>())

  val tasksUIModel = TasksUIModel(this)

  init {
    coroutineScope.launch(IODispatcher) {
      tasksUIModel.uiState
        .debounce(500)
        .collect { savedState.set("uiState", it) }
    }
    coroutineScope.launch {
      tasksSortedUseCase.tasks.collect {
        tasksUIModel.setTasks(it)
      }
    }
    coroutineScope.launch {
      tasksSortedUseCase.selectedSort.collect {
        tasksUIModel.setTasksSort(it)
      }
    }
    coroutineScope.launch {
      tasksSortedUseCase.getAllSortedIntoMutableState()
    }
  }

  fun setAndSaveTasksSort(sort: SortEnum) = coroutineScope.launch {
    tasksSortedUseCase.setTaskSort(sort)
  }

  fun insertTask(taskName: String) = coroutineScope.launch {
    tasksSortedUseCase.taskInsert(taskName)
  }

  fun insertTaskDelay(taskName: String) = coroutineScope.launch {
    delayLong(600)
    insertTask(taskName)
  }

}
