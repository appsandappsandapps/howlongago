package app.howlongago.tasklist.model

import app.howlongago.AppParcelable
import app.howlongago.AppParcelize
import app.howlongago.db.TaskWithInstance
import app.howlongago.friendlyDateAgo

@AppParcelize
public data class TaskWithInstanceDateUI(
  public val uid: Long,
  public val taskName: String?,
  public val date: Long?,
  public val inst_date: Long?
): AppParcelable {

  constructor(task: TaskWithInstance) : this(
    uid = task.uid,
    taskName = task.taskName,
    date = task.date,
    inst_date = task.inst_date,
  )

  val friendlyDate: String
    get() = inst_date?.let { friendlyDateAgo(it) } ?: "Never"

  val uidPlusDate: String
    get() = "${uid} + ${friendlyDate}"

}
