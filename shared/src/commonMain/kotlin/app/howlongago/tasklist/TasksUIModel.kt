package app.howlongago.tasklist

import app.howlongago.AppParcelable
import app.howlongago.AppParcelize
import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.db.TaskWithInstance
import app.howlongago.tasklist.model.TaskWithInstanceDateUI
import kotlinx.coroutines.flow.MutableStateFlow

@AppParcelize
data class TasksUIState(
  val newTaskInputText: String = "",
  val expandTextEntry: Boolean = false,
  val ypos: Float = 0F,
  val isSortMenuExpanded: Boolean = false,
  val sortType: SortEnum = SortEnum.DATE_ASC,
  val tasks: List<TaskWithInstanceDateUI> = listOf(),
  val forceUpdate: Int = 0,
) : AppParcelable {
  constructor(foriOSConstructor: Boolean) : this() // to give ios a default constructor
}

class TasksUIEvents(
  val tasksViewModel: TasksViewModel,
  val uiState: MutableStateFlow<TasksUIState>,
) {

  constructor() : this(TasksViewModel(), MutableStateFlow(TasksUIState())) // for stub

  fun ypos(y: Float) {
    uiState.value = uiState.value.copy(
      ypos = y
    )
  }

  fun onForceUIUpdate() {
    uiState.value = uiState.value.copy(
      forceUpdate = uiState.value.forceUpdate + 1
    )
  }

  fun onClickSortItem(sort: SortEnum) {
    tasksViewModel.setAndSaveTasksSort(sort)
    onClickSortMenuExpand(false)
  }

  fun onNewEntryInsert() {
    if (uiState.value.newTaskInputText.isNotBlank())
      tasksViewModel.insertTask(uiState.value.newTaskInputText)
    onSetNewTaskInputText("")
    onClickExpandTextEntry(false)
  }

  fun onNewEntryInsertDelay() {
    if (uiState.value.newTaskInputText.isNotBlank())
      tasksViewModel.insertTaskDelay(uiState.value.newTaskInputText)
    onSetNewTaskInputText("")
    onClickExpandTextEntry(false)
  }

  fun onSetNewTaskInputText(text: String) {
    uiState.value = uiState.value.copy(
      newTaskInputText = text
    )
  }

  fun onClickSortMenuExpand(expand: Boolean) {
    uiState.value = uiState.value.copy(
      isSortMenuExpanded = expand
    )
  }

  fun onClickExpandTextEntry(expand: Boolean) {
    uiState.value = uiState.value.copy(
      expandTextEntry = expand
    )
  }

  fun onToggleExpandTextEntry() {
    uiState.value = uiState.value.copy(
      expandTextEntry = !uiState.value.expandTextEntry
    )
  }

  fun onCancelTaskEntry() {
    onClickExpandTextEntry(false)
    onSetNewTaskInputText("")
  }

}

class TasksUIModel(val tasksViewModel: TasksViewModel) {

  val uiState = MutableStateFlow(TasksUIState())
  val uiEvents = TasksUIEvents(tasksViewModel, uiState)

  fun setTasks(tasks: List<TaskWithInstance>) {
    uiState.value = uiState.value.copy(
      tasks = tasks.map { TaskWithInstanceDateUI(it) }
    )
  }

  fun setTasksSort(sort: SortEnum) {
    uiState.value = uiState.value.copy(
      sortType = sort
    )
  }

}