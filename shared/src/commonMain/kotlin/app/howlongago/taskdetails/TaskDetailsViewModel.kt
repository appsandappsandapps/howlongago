package app.howlongago.taskdetails

import app.howlongago.*
import app.howlongago.usecases.TaskDetailsUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.debounce

/**
 * For the task detail page
 */
open class TaskDetailsViewModel(
  val savedState: StateSavable<TaskDetailsUIState>,
  val taskDetails: TaskDetailsUseCase = ServiceLocator.taskDetailsUseCase!!,
  val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + UIImmediateDispatcher),
  val ioDispatcher: CoroutineDispatcher = IODispatcher,
) : AppViewModel() {

  constructor() : this(EmptyStateSaver<TaskDetailsUIState>())

  val taskDetailsUIModel = TaskDetailsUIModel(
    this,
    savedState.get("uiState") ?: TaskDetailsUIState(),
  )

  init {
    coroutineScope.launch(IODispatcher) {
      taskDetailsUIModel.uiState
        .debounce(500)
        .collect { savedState.set("uiState", it) }
    }
    coroutineScope.launch {
      taskDetails.instances.collect {
        taskDetailsUIModel.setInstances(it)
      }
    }
    coroutineScope.launch {
      taskDetails.selectedTask.collect {
        taskDetailsUIModel.setCurrentTaskName(it.task_name)
      }
    }
  }

  fun setTask(taskUid: Long) = coroutineScope.launch {
    taskDetails.setSelectedTask(taskUid)
  }

  fun insertNewInstance(taskUid: Long) = coroutineScope.launch {
    taskDetails.insertInstance(taskUid)
    delayLong(400)
  }

  fun updateTaskNameDelayed(uid: Long, name: String) = coroutineScope.launch {
    taskDetails.updateTaskName(uid, name)
  }

  fun updateInstanceDate(taskId: Long, instanceId: Long, date: Long) = coroutineScope.launch {
    taskDetails.updateInstanceDate(taskId, instanceId, date)
  }

  fun updateInstanceNoteDelayed(taskId: Long, instanceId: Long, note: String) = coroutineScope.launch {
    taskDetails.updateInstanceNote(taskId, instanceId, note)
  }

  fun deleteTask(uid: Long) = coroutineScope.launch {
    taskDetails.deleteTask(uid)
  }

  fun deleteInstance(taskUid: Long, uid: Long) = coroutineScope.launch(ioDispatcher) {
    taskDetails.deleteInstance(taskUid, uid)
  }

}
