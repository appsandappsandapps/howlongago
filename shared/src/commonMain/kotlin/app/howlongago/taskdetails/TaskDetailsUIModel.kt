package app.howlongago.taskdetails

import app.howlongago.AppParcelable
import app.howlongago.AppParcelize
import app.howlongago.db.Instance
import app.howlongago.taskdetails.model.InstanceUI
import app.howlongago.tasklist.TasksUIState
import app.howlongago.tasklist.TasksViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@OptIn(ExperimentalMultiplatform::class)
@AppParcelize
data class TaskDetailsUIState(
  val currentTaskName: String = "",
  val instances: List<InstanceUI> = listOf(),
  val forceUpdate: Long = 0,
) : AppParcelable {
  constructor(foriOSConstructor: Boolean) : this() // ios default constructor
}

class TaskDetailsUIEvents (
  val taskDetailsViewModel: TaskDetailsViewModel,
  val uiState: MutableStateFlow<TaskDetailsUIState>,
) {

  constructor() : this(TaskDetailsViewModel(), MutableStateFlow(TaskDetailsUIState())) // for stub

  fun onReloadTask(taskId: Long) {
    taskDetailsViewModel.setTask(taskId)
  }

  fun onDeleteTask(taskId: Long) {
    taskDetailsViewModel.deleteTask(taskId)
  }

  fun onUpdateTaskName(taskId: Long, name: String) {
    uiState.value = uiState.value.copy(
      currentTaskName = name
    )
    taskDetailsViewModel.updateTaskNameDelayed(taskId, name)
  }

  fun onDeleteInstance(taskId: Long, instanceId: Long) {
    taskDetailsViewModel.deleteInstance(taskId, instanceId)
  }

  fun onForceUIUpdate() {
    uiState.value = uiState.value.copy(
      forceUpdate = uiState.value.forceUpdate + 1
    )
  }

  fun onExpandInstance(instanceId: Long) {
    val instances = uiState.value.instances.map { if(instanceId == it.uid) it.copy(expanded = !it.expanded) else it }
    uiState.value = uiState.value.copy(
      instances = instances
    )
  }

  fun onUpdateInstanceDate(taskId: Long, instanceId: Long, date: Long) {
    taskDetailsViewModel.updateInstanceDate(taskId, instanceId, date)
  }

  fun onUpdateInstanceNote(taskId: Long, instanceId: Long, note: String) {
    taskDetailsViewModel.updateInstanceNoteDelayed(taskId, instanceId, note)
  }

  fun onInsertInstanceAndExpandIt(taskId: Long) {
    taskDetailsViewModel
      .insertNewInstance(taskId)
      .invokeOnCompletion {
        val newInstances = uiState.value.instances.mapIndexed { i, v ->
          v.copy().apply {
            if(i == 0) expanded = true
          }
        }
        uiState.value = uiState.value.copy(
          instances = newInstances
        )
      }
  }
}

class TaskDetailsUIModel(
  taskDetailsViewModel: TaskDetailsViewModel,
  existingState: TaskDetailsUIState,
) {

  public val uiState = MutableStateFlow(existingState)
  public val uiEvents = TaskDetailsUIEvents(taskDetailsViewModel, uiState)

  fun setCurrentTaskName(name: String) {
    uiState.value = uiState.value.copy(
      currentTaskName = name
    )
  }

  fun setInstances(instances: List<Instance>) {
    // Convert to InstanceUI with old expanded set
    var instances = instances.map { instance ->
      val isExpanded = uiState.value.instances
        .filter { instance.uid == it.uid }
        .map { it.expanded }
        .getOrElse(0) { false }
      InstanceUI(instance).apply {
        expanded = isExpanded
      }
    }
    // Set the nextDate/inbetween date vairable
    instances = instances.let {
      it.mapIndexed { i, instance ->
        if(i < it.size - 1)
          instance.copy(nextDate = it[i+1].date)
        else
          instance
      }
    }
    // Set the updated instances
    uiState.value = uiState.value.copy(
      instances = instances
    )
  }

}