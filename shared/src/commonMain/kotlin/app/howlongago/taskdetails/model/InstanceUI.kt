package app.howlongago.taskdetails.model

import app.howlongago.AppParcelable
import app.howlongago.AppParcelize
import app.howlongago.db.Instance
import app.howlongago.friendlyDateAgo
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@OptIn(ExperimentalMultiplatform::class)
@AppParcelize
data class InstanceUI(
  var uid: Long,
  var task_id: Long,
  var date: Long,
  var notes: String,
  var nextDate: Long,
  var expanded: Boolean,
): AppParcelable {

  constructor(instance: Instance) : this(
    uid = instance.uid,
    task_id = instance.task_id,
    date = instance.date,
    notes = instance.notes,
    nextDate = instance.nextDate ?: 0L,
    expanded = false
  )

  val uidPlusExpanded: String =
    "$uid $expanded"

  val friendlyDateAgo: String
    get() = friendlyDateAgo(date)

  val notesLabel: String
    get() = if(notes.isBlank()) "Add a note" else notes

  val friendlyDateInbetween: String
    get() = nextDate.let { friendlyDateAgo(it, date, postFix = "inbetween") }

  val friendlyTime: String
    get() =
      with(Instant.fromEpochMilliseconds(date).toLocalDateTime(TimeZone.currentSystemDefault())) {
        val amPm = if (hour > 11) "pm" else "am"
        val hourAmPm = if (hour > 12) hour - 12 else hour
        val hourAmPmStr = hourAmPm.toString()
        val minute = minute.toString().padStart(2, '0')
        "${hourAmPmStr}:${minute}${amPm}"
      }

  val friendlyTime24: String
    get() =
      with(Instant.fromEpochMilliseconds(date).toLocalDateTime(TimeZone.currentSystemDefault())) {
        val hour = hour.toString().padStart(2, '0')
        val minute = minute.toString().padStart(2, '0')
        "${hour}:${minute}"
      }

  val friendlyDate: String
    get() =
      with(Instant.fromEpochMilliseconds(date).toLocalDateTime(TimeZone.currentSystemDefault())) {
        val day = dayOfWeek.name.lowercase().capitalize().let {
          if(it.length >= 3) it.substring(0, 3)
          else it
        }
        val month = month.name.lowercase().capitalize().let {
          if(it.length >= 3) it.substring(0, 3)
          else it
        }
        "${day} ${dayOfMonth} ${month} ${year}"
      }

  val friendlyDateTime: String
    get() = "$friendlyDate, $friendlyTime"

  val friendlyDateTime24: String
    get() = "$friendlyDate, $friendlyTime24"

}