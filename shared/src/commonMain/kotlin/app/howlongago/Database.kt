package app.howlongago

import app.howlongago.db.Instance
import app.howlongago.db.TaskWithInstance
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import app.howlongago.db.TasksAndInstancesQueries

expect fun tasksDbInit(context: AppContext): TasksAndInstancesQueries

@Serializable // So we can make json out of it
data class InstanceBackup (
  public val uid: Long,
  public val taskId: Long,
  public val date: Long,
  public val notes: String,
  public val nextDate: Long? = null,
)

@Serializable // So we can make json out of it
data class TaskBackup(
  public val uid: Long,
  public val taskName: String
)

@Serializable // So we can make json out of it
data class TaskWithInstancesBackup(
  val task: TaskBackup,
  val instances: List<InstanceBackup>?,
)