package app.howlongago

import app.howlongago.db.Instance
import app.howlongago.db.TaskWithInstance
import app.howlongago.db.TasksAndInstancesQueries
import kotlinx.coroutines.CoroutineDispatcher

expect abstract class AppContext
expect abstract class AppViewModel