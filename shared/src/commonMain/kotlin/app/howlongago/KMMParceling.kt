package app.howlongago

import app.howlongago.db.Instance
import app.howlongago.db.TaskWithInstance

expect interface AppParcelable

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.BINARY)
expect annotation class AppParcelize()

interface StateSavable<T : AppParcelable> {
  fun get(name: String): T?
  fun set(name: String, state: T)
}

class EmptyStateSaver<T : AppParcelable> : StateSavable<T> {
  override fun get(name: String): T? = null
  override fun set(name: String, state: T): Unit {}
}

expect open class StateSaver<T : AppParcelable> : StateSavable<T>