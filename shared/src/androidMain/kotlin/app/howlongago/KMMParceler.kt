package app.howlongago

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import app.howlongago.db.HowlongagoDatabase
import app.howlongago.db.TasksAndInstancesQueries
import com.squareup.sqldelight.android.AndroidSqliteDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.parcelize.Parceler
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.TypeParceler

actual typealias AppParcelable = Parcelable
actual typealias AppParcelize = Parcelize

actual open class StateSaver<T : AppParcelable>(
  val handle: SavedStateHandle
) : StateSavable<T> {
  override fun get(name: String): T? = handle.get<T>(name)
  override fun set(name: String, state: T) = handle.set<T>(name, state)
}