package app.howlongago

import app.howlongago.db.HowlongagoDatabase
import app.howlongago.db.TasksAndInstancesQueries
import com.squareup.sqldelight.android.AndroidSqliteDriver

actual fun tasksDbInit(context: AppContext): TasksAndInstancesQueries {
  val driver = AndroidSqliteDriver(
    schema = HowlongagoDatabase.Schema,
    context = context,
    name = "howlongagodb",
  )
  return HowlongagoDatabase(driver).tasksAndInstancesQueries
}
