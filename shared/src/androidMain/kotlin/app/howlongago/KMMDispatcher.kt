package app.howlongago

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import app.howlongago.db.HowlongagoDatabase
import app.howlongago.db.TasksAndInstancesQueries
import com.squareup.sqldelight.android.AndroidSqliteDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.parcelize.Parceler
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.TypeParceler

//actual val UIDispatcher: CoroutineDispatcher = Dispatchers.Main
//actual val UIImmediateDispatcher: CoroutineDispatcher = Dispatchers.Main.immediate
//actual val DefaultDispatcher: CoroutineDispatcher = Dispatchers.Default
actual val IODispatcher: CoroutineDispatcher = Dispatchers.IO
