# How Long Ago

Create tasks.
Track how long ago you did them.
Make notes about them.
See the intervals between occurrances.
Backup and import the database.

No Android permissions needed.

# Tech info

An Android Jetpack Compose and iPhone SwiftUI app using Kotlin Multiplatform

It uses:

* Android views -- Material Calendar and TimeDater picker -- which interact with Compose
* StateFlow (previously Live Data)
* View Models
* MVVM/MVI
* SQLDelight (previously Room)
* Custom compose views
* Compose Nav host for navigation
* Compose animations
* Light / dark mode

# TODOs

* Youtube video

### Next release

* tiles
* ui tests

### Later

* list item animation
* swipe to delete an instance
* strings in one place / translations
* widget
* status bar text color?
* enum for theme / save enum / react when system changes
* add instance from main page
* reminders
* better material date/time colours in light mode
* my contact info
* images from the camera

## Later UI

* better font sizes
* better color choosing--use typography

### Later bugs

* can change to a time in the future
* flash of no tasks on first load?
