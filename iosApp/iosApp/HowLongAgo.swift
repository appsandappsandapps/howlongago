import Foundation
import SwiftUI

struct HowLongAgo: View {
    
    @EnvironmentObject var navigationObserver: ObservableNavigation
    
    @State var showInput: Bool = false
    
    var presentSettings: Binding<Bool> {
        Binding(
            get: { navigationObserver.uiState.toggledSettings },
            set: { _ in  }
        )
    }
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            NavigationView {
                TasksList()
                    .navigationBarTitle("How Long Ago", displayMode: .large)
                    .toolbar {
                        ToolbarItem(placement: .navigationBarLeading) {
                            Button(action: {
                                showInput = !showInput
                            }) {
                                Image(systemName: "plus")
                                    .font(.system(size: 14.0))
                            }
                            .foregroundColor(.black)
                        }
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button(action: {
                                navigationObserver.uiEvents.onToggleSettings()
                            }) {
                                Image(systemName: "gearshape.fill")
                                    .font(.system(size: 14.0))
                            }
                            .foregroundColor(.black)
                            .sheet(isPresented: presentSettings) {
                               Settings()
                            }
                        }
                    }
            }
        }
        .overlay(alignment: .topLeading) {
            if showInput {
                TaskEntryPopup(
                    onClose: {
                        showInput = false
                    }
                )
                .transition(.move(edge: .top))
            }
        }
        .animation(.easeInOut(duration: 0.4), value: showInput)
    }
}
