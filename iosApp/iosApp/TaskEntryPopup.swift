import Foundation
import SwiftUI
import shared

struct TaskEntryPopup: View {
    
    @EnvironmentObject var tasksObserver: ObservableTasks
    @State var notes: String = ""
    
    @FocusState private var isFocused: Bool
    
    let onClose: () -> Void
    
    var body: some View {
        GeometryReader { ge in
        ZStack(alignment: .topLeading) {
            Color.white
                .opacity(0.01)
                .onTapGesture {
                    onClose()
                }
            VStack(alignment: .leading) {
                ZStack(alignment: .topLeading) {
                    if(notes.isEmpty) {
                        Text("🏃🏽‍♂️ Go jogging, 🦒 feed the giraffe, etc")
                            .foregroundColor(.black.opacity(0.5))
                            .font(.system(size: 20.0))
                            .padding(.all, 8)
                            .padding(.top, 2)
                    }
                    TextEditor(text: $notes)
                        .focused($isFocused)
                        .font(.system(size: 20.0))
                        .clipShape(RoundedRectangle(cornerRadius: 10.0))
                        .padding(.all, 4)
                        .opacity(notes.isEmpty ? 0.70 : 1.0)
                        .onAppear {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                                isFocused = true
                            }
                        }
                }
                Spacer()
                Button {
                    tasksObserver.uiEvents.onNewEntryInsert()
                    if(tasksObserver.uiState.newTaskInputText.isEmpty) {
                        
                    } else {
                        onClose()
                    }
                } label: {
                    HStack {
                        Spacer()
                        Text("Add")
                            .padding(.all, 5)
                            .foregroundColor(.white)
                            .background(.blue)
                            .clipShape(
                                RoundedCorner(radius: 5, corners: [.bottomRight, .topLeft])
                                    //.background(.green)
                                //AddButtonBackground()
                            )
                            .font(.system(size: 16.0))
                    }
                }
                .buttonStyle(.borderless)
            }
            .frame(width: ge.size.width * 0.75, height: UIScreen.main.bounds.height * 0.25, alignment: .topLeading)
            .background {
                ZStack(alignment: .topLeading) {
                    RoundedRectangle(cornerRadius: 5.0)
                        .stroke(.gray, lineWidth: 0.5)
                        .background {
                            ZStack(alignment: .topLeading) {
                                RoundedRectangle(cornerRadius: 5.0)
                                    .fill(.white)
                                    .shadow(radius: 1.5)
                            }
                        }
                }
            }
            .padding(.all, 16)
            .padding(.top, 32)
        }
        .onChange(of: notes) { note in
            tasksObserver.uiEvents.onSetNewTaskInputText(text: note)
        }
        }
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(
            roundedRect: rect,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius)
        )
        return Path(path.cgPath)
    }
}
