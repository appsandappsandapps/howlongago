import SwiftUI
import shared

struct InstanceRow: View {
    
    enum FocusField: Hashable { case field }
    @FocusState private var focusedField: FocusField?
    
    @EnvironmentObject var taskDetailsObserver: ObservableTaskDetails
    @EnvironmentObject var navigationObserver: ObservableNavigation
    
    var instance: InstanceUI
    var isLast: Bool = false
    var popupFrameLocation: Binding<CGRect?>
    var editingInstance: Binding<InstanceUI?>
    
    init(
         instance: InstanceUI,
         popupFrameLocation: Binding<CGRect?>,
         editingInstance: Binding<InstanceUI?>,
         isLast: Bool
    ) {
        self.instance = instance
        self.isLast = isLast
        self.popupFrameLocation = popupFrameLocation
        self.editingInstance = editingInstance
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(instance.friendlyDateAgo)
                Spacer()
            }
            .padding(.top, 4.0)
            Text(instance.notesLabel)
                .padding(.top, 1.0)
                .padding(.bottom, 5.0)
                .fixedSize(horizontal: false, vertical: true)
                .foregroundColor(instance.notes.isEmpty ? .gray.opacity(0.5) : .none )
            Text( isAmPmTime ? instance.friendlyDateTime : instance.friendlyDateTime24)
                .font(.system(size: 12.0))
             if !isLast {
                HStack {
                    Spacer()
                    Text(instance.friendlyDateInbetween)
                      .font(.system(size: 12.0))
                      .padding(EdgeInsets(top: 1.0, leading: 0.0, bottom: 0.0, trailing: 0.0))
                }
             }
        }
        .background {
            GeometryReader { gp in
                Rectangle()
                    .foregroundColor(Color.white.opacity(0.01))
                    .frame(width: gp.size.width, height: gp.size.height)
                    .onTapGesture {
                        UIApplication.shared.endEditing()
                        popupFrameLocation.wrappedValue = gp.frame(in: .global)
                        editingInstance.wrappedValue = instance
                    }
            }
        }
    }
}
