import Foundation
import SwiftUI
import shared

struct InstanceEditPopup: View {

    @EnvironmentObject var _taskDetailsObserver: ObservableTaskDetails
    @ObservedObject var keyboard: KeyboardObserver = KeyboardObserver()
    var taskDetailsUIState: TaskDetailsUIState { get { return _taskDetailsObserver.uiState } }
    var taskDetailsUIEvents: TaskDetailsUIEvents { get { return _taskDetailsObserver.uiEvents } }
    
    @State var deleteInstancePresent: Bool = false
    @State var date: Date = Date()
    @State var notes: String = ""
    @State var onClose: () -> Void
    var popupFrame: CGRect
    @Binding var instance: InstanceUI?
    var bottomOfPopup: CGFloat { get { popupFrame.minY + popupFrame.height } }
    var YIfKeyboardOverlaysInput: CGFloat { get { keyboard.minY - popupFrame.height - 1 } }
    
    func update() -> Void {
        taskDetailsUIEvents
            .onUpdateInstanceNote(
                taskId: instance!.task_id,
                instanceId: instance!.uid,
                note: notes)
        taskDetailsUIEvents
            .onUpdateInstanceDate(
                taskId: instance!.task_id,
                instanceId: instance!.uid,
                date: Int64(date.timeIntervalSince1970) * 1000)
        onClose()
    }
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            Color.white
                .opacity(0.01)
                .onTapGesture {
                    update()
                }
            VStack(alignment: .leading) {
                TextEditor(text: $notes)
                    .onAppear() { UITextView.appearance().backgroundColor = .clear }
                    .background(.gray.opacity(0.12))
                    .clipShape(RoundedRectangle(cornerRadius: 10.0))
                HStack {
                    DatePicker("", selection: $date, displayedComponents: [.date, .hourAndMinute])
                        .labelsHidden()
                        .datePickerStyle(.compact)
                        .scaleEffect(0.7, anchor: .leading)
                    Spacer()
                    Image(systemName: "trash")
                        .onTapGesture { deleteInstancePresent = true }
                        .font(.system(size: 15.0))
                        .foregroundColor(.gray)
                        .padding(.trailing, 5.0)
                }
            }
            .padding(.all, 5)
            .frame( width: popupFrame.width, height: popupFrame.height, alignment: .topLeading)
            .background { BubbleWithLeftPoint() }
            .padding(.top, bottomOfPopup > keyboard.minY && keyboard.minY != 0 ? YIfKeyboardOverlaysInput : popupFrame.minY)
            .padding(.leading, 50)
        }
        .ignoresSafeArea()
        .onChange(of: instance) { instance in
            date = Date(timeIntervalSince1970: Double(instance!.date / 1000))
            notes = instance!.notes
        }
        .animation(.spring(), value: YIfKeyboardOverlaysInput)
        .confirmationDialog("", isPresented: $deleteInstancePresent) {
            Button("Delete instance forever", role: .destructive, action: {
                taskDetailsUIEvents.onDeleteInstance(taskId: instance!.task_id, instanceId: instance!.uid)
                onClose()
            })
            Button("Keep instance", role: .cancel, action: {})
        }
        .onAppear {
            date = Date(timeIntervalSince1970: Double(instance!.date / 1000))
            notes = instance!.notes
        }
    }
}

struct BubbleWithLeftPoint: View {
    var body: some View {
        ZStack(alignment: .init(horizontal: .leading, vertical: .center)) {
            RoundedRectangle(cornerRadius: 10)
                //.stroke(Color.gray, lineWidth: 1)
                .overlay {
                    RoundedRectangle(cornerRadius: 10)
                        .fill(Color.white)
                }
                .shadow(radius: 1.0)
            Rectangle()
                .strokeBorder(.gray, lineWidth: 0.5)
                .background(.white)
                .frame(width: 20, height: 20)
                .rotationEffect(.degrees(45.0))
                .offset(x: -10, y: 0)
            Rectangle()
                .fill(.white)
                .frame(width: 20, height: 20)
                .rotationEffect(.degrees(45.0))
                .offset(x: -9, y: 0)
        }
    }
}
