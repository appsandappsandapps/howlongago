import SwiftUI
import shared

struct TaskDetailHeader: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @EnvironmentObject var _taskDetailsObserver: ObservableTaskDetails
    @EnvironmentObject var navigationObserver: ObservableNavigation
    var taskDetailsUIState: TaskDetailsUIState { get { return _taskDetailsObserver.uiState } }
    var taskDetailsUIEvents: TaskDetailsUIEvents { get { return _taskDetailsObserver.uiEvents } }
    
    @State var deleteTaskPresent: Bool = false
    var taskId: Int64 {
        get { navigationObserver.uiState.taskIdForDetailsPage }
    }
    var title: Binding<String> {
        Binding(
            get: { taskDetailsUIState.currentTaskName },
            set: { taskDetailsUIEvents.onUpdateTaskName(taskId: taskId, name: $0) }
        )
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                TextField("a task name", text: title)
                Spacer()
                Image(systemName: "trash")
                    .onTapGesture {
                        deleteTaskPresent = true
                    }
                    .font(.system(size: 14.0))
            }
        }
        .confirmationDialog("", isPresented: $deleteTaskPresent) {
            Button("Delete task and instances forever", role: .destructive, action: {
                self.presentationMode.wrappedValue.dismiss()
                taskDetailsUIEvents.onDeleteTask(taskId: taskId)
            })
            Button("Keep task", role: .cancel, action: {})
        }
    }
}
