import SwiftUI
import shared

struct TasksList: View {
    
    @Environment(\.scenePhase) var scenePhase
    
    @EnvironmentObject var navigationObserver: ObservableNavigation
    @EnvironmentObject var tasksObserver: ObservableTasks
    @EnvironmentObject var taskDetailsObserver: ObservableTaskDetails
    var tasksUIState: TasksUIState { get { tasksObserver.uiState } }
    var tasksUIEvents: TasksUIEvents { get { tasksObserver.uiEvents } }
    var shouldNavigate: Binding<Bool> {
        Binding(get: { navigationObserver.uiState.page == "detail" }, set: { _ in navigationObserver.uiEvents.goBackoListPage() } )
    }
    var input: Binding<String> {
        Binding(
            get: { tasksUIState.newTaskInputText },
            set: { tasksUIEvents.onSetNewTaskInputText(text: $0) }
        )
    }
    @State var note: String = "hmm"
    
    var body: some View {
        VStack(alignment: .leading) {
            /*
            TextField(
                "Task name",
                text: input,
                onCommit: {
                    tasksUIEvents.onNewEntryInsert()
                }
            )
            .padding(EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 0))
             */
            Rectangle().frame(width: 1, height: 1, alignment: .leading)
            if(tasksUIState.tasks.isEmpty) {
                Spacer()
                Text("No tasks yet! 😯")
                    .font(.system(size: 26.0))
                    .offset(x: 0.0, y: -10.0)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                Spacer()
            } else {
                List(tasksUIState.tasks, id: \.uid) { task in
                    VStack(alignment: .leading, spacing: 0) {
                        Text(task.taskName!)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .contentShape(Rectangle())
                            .font(.system(size: 20.0))
                        Text(task.friendlyDate)
                            .font(.system(size: 12.0))
                    }
                    .padding(EdgeInsets(top: 4, leading: 0, bottom: 4, trailing: 4))
                    .onTapGesture {
                        navigationObserver.uiEvents.onOpenDetailPage(taskId: task.uid)
                    }
                }
                .padding(0)
                .background {
                    NavigationLink(isActive: shouldNavigate) {
                        TaskDetail()
                            .navigationBarTitle("")
                            .navigationBarTitleDisplayMode(.inline)
                            .toolbar(content: {
                                ToolbarItem(placement: .principal) {
                                    TaskDetailHeader()
                                        .environmentObject(navigationObserver)
                                }
                            })
                         .environmentObject(taskDetailsObserver)
                         .environmentObject(navigationObserver)
                    } label: {
                        EmptyView()
                    }
                }
            }
        }
        .padding(0)
        .onAppear {
            tasksUIEvents.onForceUIUpdate()
        }
        .onChange(of: scenePhase) { phase in
            if phase == .active {
                tasksUIEvents.onForceUIUpdate()
            }
        }
    }
}
