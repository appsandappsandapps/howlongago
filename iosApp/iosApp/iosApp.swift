import SwiftUI
import Foundation
import shared
import UIKit

let isAmPmTime = DateFormatter.dateFormat (fromTemplate: "j", options:0, locale: Locale.current) == "h a"

@main
struct iosApp: App {
    
    let tasksObserver: ObservableTasks
    let taskDetailsObserver: ObservableTaskDetails
    let navigationObserver: ObservableNavigation
    
    init() {
        ServiceLocator().doInit(context: iOSContext())
        tasksObserver = ObservableTasks(TasksViewModel().tasksUIModel)
        taskDetailsObserver = ObservableTaskDetails(TaskDetailsViewModel().taskDetailsUIModel)
        navigationObserver = ObservableNavigation(NavigationViewModel().navigationUIModel)
    }
    
    var body: some Scene {
        WindowGroup {
            HowLongAgo()
                .environmentObject(tasksObserver)
                .environmentObject(taskDetailsObserver)
                .environmentObject(navigationObserver)
        }
    }
}

@MainActor
class ObservableTasks: ObservableObject {
    @Published public var uiState: TasksUIState = TasksUIState(foriOSConstructor: true)
    @Published public var uiEvents: TasksUIEvents = TasksUIEvents()
    
    init(_ uiModel: TasksUIModel) {
        uiEvents = uiModel.uiEvents
        uiModel.uiState.collect(collector: Collector<TasksUIState> { v in
            print("updating task uistate")
            DispatchQueue.main.async {
                self.uiState = v
            }
        }) { _ in print("finished ui state...") }
    }
}

@MainActor
class ObservableNavigation: ObservableObject {
    @Published public var uiState: NavigationUIState = NavigationUIState(foriOSConstructor: true)
    @Published public var uiEvents: NavigationUIEvents = NavigationUIEvents()
    
    init(_ uiModel: NavigationUIModel) {
        uiEvents = uiModel.uiEvents
        uiModel.uiState.collect(collector: Collector<NavigationUIState> { v in
            print("updating navigation: \(v.page)")
            DispatchQueue.main.async {
                self.uiState = v
            }
        }) { _ in print("finished ui state...") }
    }
}

@MainActor
class ObservableTaskDetails: ObservableObject {
    @Published public var uiState: TaskDetailsUIState = TaskDetailsUIState(foriOSConstructor: true)
    @Published public var uiEvents: TaskDetailsUIEvents = TaskDetailsUIEvents()
    
    init(_ uiModel: TaskDetailsUIModel) {
        uiEvents = uiModel.uiEvents
        uiModel.uiState.collect(collector: Collector<TaskDetailsUIState> { v in
            print("updating task uistate")
            DispatchQueue.main.async {
                self.uiState = v
            }
        }) { _ in print("finished ui state...") }
    }
}

class Collector<T> : Kotlinx_coroutines_coreFlowCollector {
    let callback:(T) -> Void

    init(callback: @escaping (T) -> Void) {
        self.callback = callback
    }
    
    func emit(value: Any?) async throws -> Void {
        if let v = value as? T {
            callback(v)
        }
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(
            #selector(UIResponder.resignFirstResponder),
                to: nil,
                from: nil,
                for: nil
        )
    }
}

extension AnyTransition {
    static var moveInAndOut: AnyTransition {
        .asymmetric(
            insertion: .move(edge: .leading),
            removal: .move(edge: .trailing)
        )
    }

}

//TODO: Cleanup
struct DocumentPicker: UIViewControllerRepresentable {
    @Binding var fileContent: String
    
    func makeCoordinator() -> DocumentPickerCoordinator {
        return DocumentPickerCoordinator(fileContent: $fileContent)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<DocumentPicker>) -> UIDocumentPickerViewController {
        let controller = UIDocumentPickerViewController(forOpeningContentTypes: [.item], asCopy: true)
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIDocumentPickerViewController, context: Context) {}
}

//TODO: Cleanup
class DocumentPickerCoordinator: NSObject, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    @Binding var fileContent: String
    
    init(fileContent: Binding<String>) {
        _fileContent = fileContent
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        do {
            fileContent = try String(contentsOf: urls[0], encoding: .utf8)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

//TODO: Cleanup
struct DocumentPickerSave: UIViewControllerRepresentable {
    
    @Binding var toSave: String
    @Binding var backedup: Bool
    
    func makeCoordinator() -> DocumentPickerCoordinatorSave {
        return DocumentPickerCoordinatorSave(toSave: $toSave, backedup: $backedup)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<DocumentPickerSave>) -> UIDocumentPickerViewController {
        let controller = UIDocumentPickerViewController(forOpeningContentTypes: [.folder], asCopy: false)
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIDocumentPickerViewController, context: Context) {}
}

//TODO: Cleanup
class DocumentPickerCoordinatorSave: NSObject, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    @Binding var toSave: String
    @Binding var backedup: Bool
    
    init(toSave: Binding<String>, backedup: Binding<Bool>) {
        self._toSave = toSave
        self._backedup = backedup
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd-HH-mm"
        let date = dateFormatterPrint.string(from: Date())
        do {
           let _ = url.startAccessingSecurityScopedResource()
           let url = url.appendingPathComponent("how_long_ago_db_ios_\(date).txt")
           try toSave.write(to: url, atomically: true, encoding: .utf8)
           url.stopAccessingSecurityScopedResource()
            backedup = true
        } catch {
           print(error.localizedDescription)
        }
    }
    
}

class KeyboardObserver: ObservableObject {
    private var center: NotificationCenter
    @Published var minY: CGFloat = 0
    
    init(center: NotificationCenter = .default) {
        self.center = center
        self.center.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        self.center.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }

    deinit {
        self.center.removeObserver(self)
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo as? [String: Any] else {
            return
        }
        guard let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        minY = keyboardInfo.cgRectValue.minY
    }
    
    @objc func keyboardDidHide(_ notification: Notification) {
        minY = 0
    }
}
